# Caesium library
All kinds of different helper functions and scripts developed for and in
the Caesium lab.

## Installation
You can install this package via pip by running
```
pip install git+https://gitlab.physik.uni-muenchen.de/cs/cs-lib.git
```

### Dependency
To list the cs-lib as dependency for setuptools you need to add the download
URL as follows:
```
cs-lib @ git+https://gitlab.physik.uni-muenchen.de/cs/cs-lib.git
```

### Development
To install this library clone it, switch into the newly created folder and
run
```
python3 setup.py develop
```
You will need Python 3.5 or newer to run this library, as well as some
dependencies which should be installed automatically.

## Namespace
This library is set up so that `cs` is a namespace and can be used by other
packages as well (e.g. `cs.analyzer`). It uses the method defined in [PEP
420][0], working since python 3.3. The only trick is that there is no
`__init__.py` file in the the namespace directory (in this case the `cs`
subfolder). This must be used by all packages using this namespace.


[0]: https://www.python.org/dev/peps/pep-0420/
