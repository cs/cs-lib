# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a
Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
* ABCDEF: the function returned from propagation now has a `length` property,
  returning the total length of the passed elements

## [1.1.0] - 2021-10-12
### Changed
* ABCDEF: start position can be specified and plot style can be adjusted

### Added
* new lattice code `cs.lattice.bloch`. Slightly optimised version of Tills
  code from the shortReport notebook.
* ABCDEF: new object `skipSpace` which does not perform propagation within
  the specified section

## [1.0.0] - 2021-09-28
Tagged version 1.0.0 at some point in time to get to proper versioning
