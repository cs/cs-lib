#!/usr/bin/env python3
# coding: utf-8
"""
@author Hendrik v. Raven

Function to compute the magnetic field generated by currents flowing through
wires, i.e. coils. Supports round coils and straight wire pieces.
"""

import attr
import matplotlib.pyplot as plt
import numpy as np
from scipy.special import ellipe, ellipk

from cs.constants import mu0, pi
from cs.units import A, H, Quantity, T, W, m, quantity_input, unit_attr
from cs.vector import get_cos_angle, in_translated_rotated_reference_frame, normalized

# we will trigger div by zero and invalid multiplications
np.seterr(all="ignore")


# # Calculation for coil loops
# Based on Phys. Rev. A 35 1535 eqs. (1) and (2). Computation is done for a
# coil around the origin in polar coordinates and then translated into the
# correct frame.
@quantity_input
def coil_polar(r: m, z: m, R: m):
    """
    Compute the magnetic field of a single wire loop in the xy-plane with the
    center at the origin. Evaluation is done in polar coordinates.

    Based on PhysRevA 35 1535 eqs. (1) and (2)

    Parameters
    ==========
        r: np.ndarray
            radius for evaluation
        z: np.ndarray
            vertical distance from coil for evaluation.
            Must be of same shape as r.
        R: float
            coil radius
    Returns
    ======
        Br, Bz: (np.ndarray, np.ndarray)
            The resulting magnetic field radial and vertial components. Indices
            match with the given input coordinates.
    """
    elliparg = 4 * R * r / ((R + r) ** 2 + z ** 2)
    E = ellipe(np.asarray(elliparg.si))
    K = ellipk(np.asarray(elliparg.si))

    Bz = (
        mu0
        / (2 * pi * np.sqrt((R + r) ** 2 + z ** 2))
        * (K + E * (R ** 2 - r ** 2 - z ** 2) / ((R - r) ** 2 + z ** 2))
    )
    Br = (
        mu0
        * z
        / (2 * pi * r * np.sqrt((R + r) ** 2 + z ** 2))
        * (-K + E * (R ** 2 + r ** 2 + z ** 2) / ((R - r) ** 2 + z ** 2))
    )
    # on the coil radius we get a division through 0, fix this
    Br = np.nan_to_num(Br)
    return Br, Bz


@quantity_input
def coil_cartesian(coords: m, R: m) -> T / A:
    phi = np.sqrt(coords[0] ** 2 + coords[1] ** 2)

    Bphi, Bz = coil_polar(phi, coords[2], R)

    Bx = np.nan_to_num(coords[0] / phi * Bphi)
    By = np.nan_to_num(coords[1] / phi * Bphi)

    assert Bz.unit.is_equivalent(T / A)
    assert Bx.unit.is_equivalent(T / A)

    return Quantity((Bx, By, Bz))


@quantity_input
def coil(coords: m, position: m, normal, radius: m) -> T / A:
    """
    computes the magnetic field generated by a single coil winding

    Parameters
    ----------
        x, y, z: ndarray
            Arbitrarily shaped vectors containing the different coordinate
            components such as generated by np.meshgrid
        position: ndarray, shape(3, )
            Vector pointing to the center position of the coil
        normal: ndarray, shape(3, )
            Normal vector orthogonal to the plane of the coil
        radius: float
            Radius of the coil (in meters)

    Returns
    -------
        Bx, By, Bz: ndarray
            Magnetic field components at the locations specified by x, y, z.
            Magnetic field is given in SI units, i.e. T/m for a current of 1A.
    """
    return in_translated_rotated_reference_frame(
        position, normal, coords, coil_cartesian, radius
    )


class GenericCoil(object):
    @property
    def resistance(self):
        if not hasattr(self, "wire"):
            raise Exception(
                "need a wire to compute the resistance! Set coil.wire first"
            )
        return self.length * self.wire.resistivity

    @quantity_input
    def dissipated_power(self, current: A) -> W:
        return self.resistance * current ** 2

    @property
    def inductance(self) -> H:
        return (mu0 * self.windings ** 2 * self.area / self.length).to(H)


@attr.s
class Coil(GenericCoil):
    position = attr.ib(converter=Quantity)
    normal = attr.ib()
    radius = attr.ib()
    windings = attr.ib(default=1)

    @quantity_input
    def evaluate(self, coords: m) -> T / A:
        # radius can be used to specify an inner and outer radius
        # in this case the windings are distributed along that radius
        try:
            turns = np.linspace(*self.radius, num=self.windings)
            B = np.zeros_like(coords) * 1 / m * T / A
            for r in turns:
                B += coil(coords, self.position, self.normal, r)
            return B
        except TypeError:
            # presumable windings is not iterable
            return self.windings * coil(coords, self.position, self.normal, self.radius)

    @property
    def length(self):
        return 2 * pi * self.radius * self.windings

    @property
    def area(self):
        return pi * self.radius ** 2


# # Finite wire
# Magnetic field of a finite wire given as
# $$ B = \frac{mu0I}{4pid}\left(\cos{theta} + \cos{phi}\right) $$
# with $d$ the distance to the wire and $theta$ and $phi$ the angles to the start
# and end point of the wire.
@attr.s
class StraightWire(GenericCoil):
    start = unit_attr(m)
    end = unit_attr(m)

    @quantity_input
    def evaluate(self, coords: m) -> T / A:
        if np.all(np.isclose(self.start.si.value, self.end.si.value)):
            return np.zeros((coords.shape)) * T / A

        # we need 3d coordinates
        pos = coords.T.si.value
        start = self.start.si.value
        end = self.end.si.value
        k = np.cross(end - start, start - pos)
        d = np.linalg.norm(k, axis=-1) / np.linalg.norm(end - start, axis=-1)
        cos_theta = get_cos_angle(end, start, pos)
        cos_phi = get_cos_angle(start, end, pos)
        assert d.shape == cos_theta.shape
        assert cos_theta.shape == cos_phi.shape
        B0 = mu0.si.value / (4 * pi * d) * (cos_theta + cos_phi)
        B = normalized(k) * np.expand_dims(B0, axis=-1)
        return B.T * T / A

    @property
    def length(self):
        return np.linalg.norm((self.end - self.start).si.value) * m


@attr.s
class SquareCoil(GenericCoil):
    position = unit_attr(m)
    normal = attr.ib(converter=np.asanyarray)
    dx = unit_attr(m)
    dy = unit_attr(m)
    windings = attr.ib(default=1)
    corner_radius = attr.ib(default=None)
    wires_per_corner = attr.ib(default=8)

    def _rect_wires(self):
        dx = self.dx
        dy = self.dy
        return [
            Quantity([-dx / 2, -dy / 2, 0 * m]),
            Quantity([-dx / 2, dy / 2, 0 * m]),
            Quantity([dx / 2, dy / 2, 0 * m]),
            Quantity([dx / 2, -dy / 2, 0 * m]),
            Quantity([-dx / 2, -dy / 2, 0 * m]),
        ]

    def _corner_wires(self):
        dx = self.dx
        dy = self.dy
        r = self.corner_radius
        wires = self.wires_per_corner

        pos = [Quantity([-dx / 2, -dy / 2 + r, 0 * m])]

        corners = [(-1, 1), (1, 1), (1, -1), (-1, -1)]
        for i, corner in enumerate(corners):
            for j in np.linspace(0, np.pi / 2, wires + 1, endpoint=True):
                phi = j + i * np.pi / 2
                pos.append(
                    Quantity(
                        [
                            corner[0] * (dx / 2 - r) - r * np.cos(phi),
                            corner[1] * (dy / 2 - r) + r * np.sin(phi),
                            0 * m,
                        ]
                    )
                )
        return pos

    @property
    def wires(self):
        if self.corner_radius is not None:
            wires = self._corner_wires()
        else:
            wires = self._rect_wires()
        return map(lambda x: StraightWire(*x), zip(wires[:-1], wires[1:]))

    @quantity_input
    def evaluate(self, coords: m) -> T / A:
        def doeval(cs) -> T / A:
            return (
                np.sum([w.evaluate(cs).to(T / A).value for w in self.wires], axis=0)
                * self.windings
                * T
                / A
            )

        return in_translated_rotated_reference_frame(
            self.position, self.normal, coords, doeval
        )

    @property
    def length(self):
        l = np.sum([w.length.to_value(m) for w in self.wires]) * m
        return l * self.windings

    @property
    def area(self):
        if self.corner_radius is None:
            return self.dx * self.dy
        else:
            return self.dx * self.dy - (4 - np.pi) * self.corner_radius ** 2


def evaluate(coils, volume):
    B = np.zeros_like(volume) * (T / m / A)
    for c in coils:
        B += c.evaluate(volume)
    return B


def analyse_at_center(x, y, z, B):
    center = np.logical_and(x == 0, np.logical_and(y == 0, z == 0))
    B0 = np.linalg.norm(B, axis=0)[center]
    dB = np.zeros(3)
    for i in [0, 1, 2]:
        # BUG this needs to be divided by the distance between the sampling points
        dB[i] = np.gradient(B[i], axis=i)[center]
    return B0, dB


def compute_field(coils, position):
    B = np.zeros_like(position) * (1 / m * T / A)
    for c in coils:
        B += c.evaluate(position)
    return B


def compute_gradient(coils, position, step_size=1e-3 * m):
    x, y, z = np.mgrid[-1:1:3j, -1:1:3j, -1:1:3j] * step_size
    B = compute_field(
        coils, Quantity([x + position[0], y + position[1], z + position[2]])
    )
    dB = np.zeros(3) * T / A / m
    for i in range(3):
        dB[i] = np.gradient(B[i], axis=i)[1, 1, 1] / step_size
    return dB


def plot(coils, width=0.2 * m, samples=41):
    x, y, z = (
        np.mgrid[-1 : 1 : samples * 1j, -1 : 1 : samples * 1j, -1 : 1 : samples * 1j]
        * width
    )
    B = evaluate(coils, [x, y, z])

    def xz(orig):
        return orig[y == 0].reshape((samples, samples))

    plt.streamplot(xz(z), xz(x), xz(B[2]), xz(B[0]))


if __name__ == "__main__":
    from astropy.units import cm

    pos = np.mgrid[-1:1:11j, -1:1:11j, -1:1:11j] * cm
    print(np.asanyarray(pos).shape)
    # vals = Coil((0,0,0)*m, (0,0,1), radius=10*cm).evaluate(pos)
    vals = SquareCoil((0, 0, 0) * m, (0, 0, 1), dx=10 * cm, dy=20 * cm).evaluate(pos)
    print(vals)
