import attr
import numpy as np

import cs.units as u


@attr.s
class Material(object):
    resistivity = attr.ib()


Cu = Material(resistivity=1.68e-8 * u.ohm * u.m)


@attr.s
class Wire(object):
    material = attr.ib(default=Cu)

    @property
    def resistivity(self) -> u.ohm / u.m:
        return (self.material.resistivity / self.area).to(u.ohm / u.m)


@attr.s
class RoundWire(Wire):
    diameter = attr.ib(default=1 * u.mm)

    @property
    def width(self):
        return self.diameter

    @property
    def height(self):
        return self.diameter

    @property
    def area(self):
        return np.pi * (self.diameter / 2) ** 2


@attr.s
class SquareWire(Wire):
    width = attr.ib(default=1 * u.mm)
    height = attr.ib(default=1 * u.mm)

    @property
    def area(self):
        return self.width * self.height


@attr.s
class HollowCoreWire(SquareWire):
    core_diameter = attr.ib(default=0.5 * u.mm)

    @property
    def area(self):
        return self.width * self.height - np.pi * (self.core_diameter / 2) ** 2
