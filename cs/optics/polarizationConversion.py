import numpy as np


def polarizationConv(aR=1, psi=0, theta=0, phi=0):
    """convert polarization in laser frame into polarization in atomic frame

    Args:
        aR : float
            the amplitude of the right-handed circular component of the light in
            the laser frame. You need -1 < aR < 1. The amplitude of the left-handed
            circular components is computed via aL = sqrt(1-aR^2).
        psi : float
            the angle of the major axis of the polarization with the laser frames
            x-axis. If the light is purely left or right handed, psi is meaningless.
            psi should be in radians.
        theta : float
            the rotation angle of the laser frame around the x-axis of the atomic
            frame in radians
        phi : float
            the rotation angle of the laser frame around the z-axis of the atomic
            frame in radians. The laser frame is first rotated around the x-axis,
            then around the z-axis.

            Note that for theta = 0, phi and psi have the same meaning effect on
            the polarization and can cancel each other. They do not have the same
            meaning though.

    Returns:
        az : complex
            the pi-polarization component in the atomic frame of reference
        ap : complex
            the sigma plus-polarization component in the atomic frame
        am : complex
            the sigma minus-polarization component in the atomic frame

        If theta = 0 and aR = 0 the light is sigma plus polarized.
        az, ap and am should be normalized such that
            |az|^2 + |ap|^2 + |am|^2 = 1
    """
    assert (
        np.abs(aR) <= 1
    ), "Amplitude of right handed circular light too large, require aR^2 + aL^2 = 1"

    norm = 1 / np.sqrt(2)
    if np.isclose(aR, np.sqrt(2)):
        tmpMinus = -1j * np.sin(psi) / norm
        tmpPlus = np.cos(psi) / norm
    else:
        aL = np.sqrt(1 - aR ** 2)
        tmpMinus = aR * np.exp(-1j * psi) - aL * np.exp(1j * psi)
        tmpPlus = aR * np.exp(-1j * psi) + aL * np.exp(1j * psi)

    az = norm * 1j * tmpMinus * np.sin(theta)
    ap = np.exp(-1j * phi) / 2 * (tmpPlus + tmpMinus * np.cos(theta))
    am = np.exp(1j * phi) / 2 * (tmpPlus - tmpMinus * np.cos(theta))

    return az, ap, am


if __name__ == "__main__":

    az, ap, am = polarizationConv()
    print("Testing complete right-handed beam with no rotation:")
    res = "Test suceeded." if np.isclose(1, ap) else "Test failed."
    print(res, "az, ap, am: {:.1f}, {:.1f}, {:.1f}".format(az, ap, am))
    # check normalization:
    print("Normalized?: ", np.isclose(abs(az) ** 2 + abs(ap) ** 2 + abs(am) ** 2, 1))

    az, ap, am = polarizationConv(aR=1 / np.sqrt(2), psi=np.pi / 4)
    print("Testing linear polarized beam with rotation by 45°:")
    test = (
        np.isclose(1 / np.sqrt(2), abs(ap))
        and np.isclose(1 / np.sqrt(2), abs(am))
        and np.isclose(np.angle(ap), -np.pi / 4)
    )
    res = "Test suceeded." if test else "Test failed."
    print(res, "az, ap, am: {:.1f}, {:.1f}, {:.1f}".format(az, ap, am))
    # check normalization:
    print("Normalized?: ", np.isclose(abs(az) ** 2 + abs(ap) ** 2 + abs(am) ** 2, 1))

    az, ap, am = polarizationConv(aR=1 / np.sqrt(2), psi=0.1, phi=-0.1)
    print(
        "Testing linear polarized beam with cancelling rotation in Light Frame and"
        " Atomic Frame:"
    )
    test = (
        np.isclose(1 / np.sqrt(2), abs(ap))
        and np.isclose(1 / np.sqrt(2), abs(am))
        and np.isclose(np.angle(ap), 0)
    )
    res = "Test suceeded." if test else "Test failed."
    print(res, "az, ap, am: {:.4f}, {:.4f}, {:.4f}".format(az, ap, am))
    # check normalization:
    print("Normalized?: ", np.isclose(abs(az) ** 2 + abs(ap) ** 2 + abs(am) ** 2, 1))

    az, ap, am = polarizationConv(aR=1 / np.sqrt(2), psi=np.pi / 2, theta=np.pi / 2)
    print(
        "Testing linear polarized beam (along LF y-axis) with LF rotation by 90° around"
        " AF x-axis:"
    )
    test = np.isclose(1, abs(az))
    res = "Test suceeded." if test else "Test failed."
    print(res, "az, ap, am: {:.1f}, {:.1f}, {:.1f}".format(az, ap, am))
    # check normalization:
    print("Normalized?: ", np.isclose(abs(az) ** 2 + abs(ap) ** 2 + abs(am) ** 2, 1))
