import numpy as np

import cs.constants as pc
import cs.units as u


def rayleighLength(waist, wavelength):
    return np.pi * waist ** 2 / wavelength


zR = rayleighLength


def gaussianWaist(waist, wavelength):
    def w(z):
        return waist * np.sqrt(1 + (z / zR(waist, wavelength)) ** 2)

    return w


def gaussianIntensity(power, waist):
    return 2 * power / np.pi / waist ** 2


I0 = gaussianIntensity


def gaussianBeam(power, waist, wavelength):
    def b(r, z):
        wz = gaussianWaist(waist, wavelength)
        return I0(power, waist) * (waist / wz(z)) ** 2 * np.exp(-2 * r ** 2 / wz ** 2)

    return b
