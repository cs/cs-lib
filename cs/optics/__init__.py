import numpy as np

from cs.units import m, quantity_input


@quantity_input
def rayleigh_length(waist: m, wavelength: m):
    return (np.pi * waist ** 2 / wavelength).si
