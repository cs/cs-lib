"""
TODO compute period and vdepth from a laserBeam instance
TODO update computations appropriately if period, atom, nx, nsites or
     vdepth changes
TODO find a better way to include ascatt. set to None? Currently has
     to be set by hand via BlochLattice.ascatt = a
TODO better interface to extract Bloch functions and Wannier functions
     so one does not have to think about which are the right indices
TODO superlattices might be interesting and the inclusing is 
     relatively easy (add higher order Fourier components to V0)
"""
import numpy as np
import scipy.constants as pc


class BlochLattice(object):
    """class for computing Bloch and Wannier functions

    the code assumes that the potential/lattice is sinusoidal.
    """

    def __init__(self, atom, period, vdepth, nx=32, nsites=32):
        """instantiate new BlochLattice

        Args:
            atom : atom.classes.atom instance
                the atom sitting in the lattice
            period : float
                the period of the Bloch lattice e.g. half the
                wavelength
            vdepth : float
                the depth of the Bloch lattice
            nx : int
                the desired sampling of the lattice unit cell
            nsites : int
                the desired number of lattice sites to use

        Note:
            nx and nsites determine the number of eigenvalues
            computed, the reciprocal lattice vectors (nx) and the
            sampling of the Brilluoin zone (nsites)

        The band structure, Bloch periodic functions ux,
        eigenfunctions phi of the Hamiltonian and Wannier functions
        are automatically computed when instantiating the class.
        """
        self.atom = atom
        self.period = period
        self.nx = nx
        self.nsites = nsites
        self.vdepth = vdepth

        self.k = self.fourierAxis(self.nx, self.period / self.nx)
        self.q = self.fourierAxis(self.nsites, period)
        self.x = np.arange(-self.nx / 2, self.nx / 2) * period
        self.xtot = (
            np.arange(-(self.nsites + 1) * self.nx / 2, (self.nsites - 1) * self.nx / 2)
            * self.period
            / self.nx
        )

        self.v0 = -0.25 * self.vdepth * (np.eye(self.nx, k=-1) + np.eye(self.nx, k=1))

        self.computeBlochBands()
        self.computeWannierFunc()

    @property
    def J(self):
        """the coupling between adjacent sites (in Joule)"""
        return 0.25 * (np.max(self.bands, axis=0) - np.min(self.bands, axis=0))

    @property
    def U(self):
        """the onsite interaction energy (in Joule)"""
        try:
            pc.h ** 2 / np.pi / self.atom.m / self.ascatt * (
                np.sum(np.abs(self.wannier) ** 2, axis=0) * self.period / self.nx
            ) ** 3
        except:
            raise NameError(
                "Missing something, most likely ascatt. Set with "
                "OpticalLattice.ascatt = ascatt"
            )

    def computeBlochBands(self):
        """compute the bandstructure and Bloch eigenstates

        creates three new attributes of the class:
            self.bands : numpy.ndarray
                the bandstructue (Joule), shape: (nx,nx). The columns
                of self.bands give the q-dependence of a given energy
                level, the rows give the energy eigenvalues for a
                given q
            self.ux : numpy.ndarray
                the Bloch periodic part of the eigenfunctions,
                shape: (nsites, nx, nx). self.ux[i,:,j] give the real
                space dependence of the Bloch periodic function in the
                unit cell for a particle with quantum numbers (q[i],j)
                where j is the band index and q a reciprocal lattice
                vector
            self.phi : numpy.ndarray
                the eigenfunctions of the particle.
                shape: (nsites, nx, nx). Roughly
                    self.phi = self.ux*exp(1j * self.q * self.x)
        """
        self.bands = np.zeros((self.nsites, self.nx))
        self.ux = np.zeros((self.nsites, self.nx, self.nx), dtype="complex")

        for ind, q in enumerate(self.q):
            # fourier space representation of the hamiltonian
            e, w = np.linalg.eig(
                np.diag(self.k - q) ** 2 * pc.hbar ** 2 / 2 / self.atom.m.value
                + self.v0.value
            )
            ii = np.argsort(e)  # sort such that low eigenvalues come first
            e, w = e[ii], w[:, ii]

            tmp = self.ft(w)  # real space representation of eigenfunctions
            tmp = self.normalize(tmp, 1 / self.period)

            # sign adjustment necessary for Wannier function computation
            tmp[:, np.where(np.mean(tmp, axis=0) > 0)] *= -1

            self.bands[ind] = e
            self.ux[ind] = tmp
        self.phi = self.ux * np.transpose(
            np.tile(np.exp(1j * np.outer(self.q, self.x)), (self.nx, 1, 1)), (1, 2, 0)
        )

    def computeWannierFunc(self):
        """compute the Wannier function of the Bloch lattice

        creates a new attribute:
            self.wannier : numpy.ndarray
                the wannier functions of the lattice.
                shape: (nsites, nx*nsites, nx)
                The second dimension (giving the spatial dependence)
                of the array is larger than the Bloch period array
                self.ux, since the Wannier functions are not periodic
                but have to be represented on the whole lattice. The
                whole lattice has nsites * nx points, since each unit
                cell contains nx points.
                self.wannier[j,:,n] gives the Wannier function in
                the n-th band at site j.
        """
        xsites = np.arange(-self.nsites / 2, self.nsites / 2) * self.period
        tmp = np.tile(self.ux, (1, self.nsites, 1))
        self.wannier = np.zeros(tmp.shape) + 0j

        for ind, xs in enumerate(xsites):
            tmp2 = np.sum(
                tmp
                * np.transpose(
                    np.tile(
                        np.exp(1j * np.outer(self.q, self.xtot - xs)), (self.nx, 1, 1)
                    ),
                    (1, 2, 0),
                ),
                axis=0,
            )
            self.wannier[ind] = self.normalize(tmp2, self.period / self.nx)

    @staticmethod
    def normalize(a, dx, axis=0):
        """normalize the array a assuming a sampling dx"""
        return a / np.sqrt(np.sum(np.abs(a) ** 2, axis=axis) * dx)

    @staticmethod
    def ft(a):
        """compute the fft of array a along axis 0"""
        return np.fft.ifftshift(np.fft.fft(np.fft.fftshift(a, axes=0), axis=0), axes=0)

    @staticmethod
    def fourierAxis(nx, dx):
        """compute the axis for the Fourier transform

        Args:
            nx : int
                the number of points of the original axis
            dx : float
                the sampling of the original axis
        """
        return np.fft.fftshift(np.fft.fftfreq(nx, dx)) * 2 * np.pi


if __name__ == "__main__":

    import astropy.units as u
    import matplotlib.pyplot as plt

    import cs.atom.caesium as caesium

    normMax = lambda a: a / np.max(np.abs(a))  # normalize to max(abs(a)) = 1

    Er = caesium.caesium.recoil_energy(
        2 * np.pi * pc.c / 1064e-9 * u.Hz
    )  # recoil energy
    bl = BlochLattice(caesium.caesium, 1064e-9 / 2, 8 * Er)  # create the bloch lattice

    # plot first three bands, two Bloch functions and three Wannier functions
    fig = plt.figure(figsize=(15, 7))
    fig.add_subplot(131)
    plt.plot(bl.q / 1e6, bl.bands[:, :3] / Er)
    plt.title("Band structure")
    plt.xlabel("q (1/µm)")
    plt.ylabel(r"$E_n(q)/E_r$")
    fig.add_subplot(132)
    plt.plot(bl.x * 1e6, normMax(np.abs(bl.phi[[0, 16], :, 0])).T)
    plt.title("Bloch function, periodic part")
    plt.xlabel("x (µm, single lattice period)")
    plt.ylabel(r"$|u_{n=0,q=-k,0}(x)|^2$ (not normalized)")
    fig.add_subplot(133)
    plt.plot(
        bl.xtot * 1e6,
        normMax(np.abs(bl.wannier[15:18, :, 0].T)),
        bl.xtot * 1e6,
        np.sin(np.pi * bl.xtot / bl.period) ** 2,
        ":",
    )
    plt.xlim([-3 * bl.period * 1e6, 3 * bl.period * 1e6])
    plt.title("Wannier function")
    plt.xlabel("x (µm)")
    plt.ylabel(r"$|w_{n=0}(x)|^2$ for adjacent sites (not normalized)")
    plt.show()
