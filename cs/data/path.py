import os

CS_DATA_PATH = os.environ.get("CS_DATA_PATH")
if not CS_DATA_PATH:
    if os.name == "nt":
        # on windows we mount it usually as Z:
        CS_DATA_PATH = r"Z:\\"
    else:
        CS_DATA_PATH = "/mnt/data"


CS_MEASUREMENTS_PATH = os.path.join(CS_DATA_PATH, "measurements")
