import datetime
import pathlib

import pandas as pd
import parsedatetime


def load_measurement(date=None, time=None, label="", identifier="*", exact=False):
    """
    open a measurement identified by the functions parameters and return
    it as a Pandas HDFStore. Tries to detect the correct file based on
    the inserted parameters, uses the latest file where information is
    missing. Fuzzing can be disabled by setting exact to True.
    """
    return pd.HDFStore(
        fuzzy_find_measurement(date, time, label, identifier, exact), "r"
    )


def fuzzy_find_measurement(date=None, time=None, label="", identifier="*", exact=False):
    """
    open a measurement identified by the functions parameters and return
    it as a Pandas HDFStore. Tries to detect the correct file based on
    the inserted parameters, uses the latest file where information is
    missing. Fuzzing can be disabled by setting exact to True.
    """

    if not date:
        date = datetime.datetime.today()
    elif isinstance(date, str):
        date = datetime.datetime(*parsedatetime.Calendar().parse(date)[0][:6])

    folder = [CS_DATA_PATH, "measurements", date.strftime("%Y/%m/%d"), label]
    path = pathlib.Path("/".join(folder))

    if not path.exists():
        raise FileNotFoundError(f"Directory {path} does not exist")

    if time:
        parsed_time = datetime.datetime(*parsedatetime.Calendar().parse(time)[0][:6])
        time = parsed_time.strftime("%H%M%S_")
    elif not exact:
        time = ""
    else:
        raise Exception("Time not specified, but exact handling forced")

    filename_fuzzer = f"**/{date.strftime('%Y%m%d')}_{time}{identifier}.hdf5"

    files = list(path.glob(filename_fuzzer))

    if len(files) == 0:
        raise FileNotFoundError(
            f"Found no matching files in {path} for time={time}, label={label},"
            f" identifier={identifier}, glob={filename_fuzzer}"
        )

    if len(files) == 1:
        return files[0]

    # if time was not specified we use the latest matching file
    if not time:
        return sorted(files)[-1]

    else:
        raise Exception(f"Found multiple matching files: {files}")
