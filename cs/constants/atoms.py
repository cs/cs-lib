#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  3 12:53:40 2017

@author: Hendrik v. Raven

Physical constants to be used throughout the scripts.
All values are given in SI units.
"""

import attr
from astropy.constants import Constant, a0
from numpy import array, sqrt

import cs.units as u


@attr.s
class Transition:
    f = attr.ib()
    wavelength = attr.ib()
    gamma = attr.ib()
    tau = attr.ib()


class ConstantContainer:
    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)


class Caesium:
    d2 = Transition(
        f=Constant(
            abbrev="f",
            value=351.72571850,
            unit=u.THz,
            uncertainty=11e-8,
            name="Transition Frequency",
            reference="D. Steck, Cesium D Line Data, revision 1.6",
        ),
        wavelength=Constant(
            abbrev="lambda",
            value=852.34727582,
            unit=u.nm,
            uncertainty=270e-9,
            name="Wavelength in Vacuum",
            reference="D. Steck, Cesium D Line Data, revision 1.6",
        ),
        gamma=Constant(
            abbrev="gamma",
            value=5.2227,
            unit=u.MHz,
            uncertainty=6.6e-3,
            name="Natural Line Width (FWHM)",
            reference="D. Steck, Cesium D Line Data, revision 1.6",
        ),
        tau=Constant(
            abbrev="tau",
            value=30.473e-9,
            unit=u.s,
            uncertainty=0.039e-9,
            name="Lifetime",
            reference="D. Steck, Cesium D Line Data, revision 1.6",
        ),
    )
    d2.f4f5_circ = ConstantContainer(
        d=Constant(
            abbrev="d",
            value=2.6850e-29,
            unit=u.C * u.m,
            uncertainty=2.4e-32,
            name=(
                "Dipole Moment (F4,mF4 -> F5,mF5 cycling transition, sigma polarized"
                " light)"
            ),
            reference="D. Steck, Cesium D Line Data, revision 1.6",
        ),
        I_sat=Constant(
            abbrev="I_sat",
            value=1.1023,
            unit=u.mW / u.cm ** 2,
            uncertainty=1e-3,
            name=(
                "Saturation Intensity(F4,mF4 -> F5,mF5 cycling transition, sigma"
                " polarized light)"
            ),
            reference="D. Steck, Cesium D Line Data, revision 1.6",
        ),
        sigma0=Constant(
            abbrev="sigma0",
            value=3.469e-9,
            unit=u.cm ** 2,
            uncertainty=0,
            name=(
                "Resonant cross section (F4,mF4 -> F5,mF5 cycling transition, sigma"
                " polarized light)"
            ),
            reference="D. Steck, Cesium D Line Data, revision 1.6",
        ),
    )
    d2.f4f5_isopol = ConstantContainer(
        d=Constant(
            abbrev="d",
            value=1.7138e-29,
            unit=u.C * u.m,
            uncertainty=1.5e-32,
            name="Dipole Moment (F4 -> F5 transition, isotropically polarized light)",
            reference="D. Steck, Cesium D Line Data, revision 1.6",
        ),
        I_sat=Constant(
            abbrev="I_sat",
            value=2.7059,
            unit=u.mW / u.cm ** 2,
            uncertainty=2.4e-3,
            name=(
                "Saturation Intensity (F4 -> F5 transition, isotropically polarized"
                " light)"
            ),
            reference="D. Steck, Cesium D Line Data, revision 1.6",
        ),
        sigma0=Constant(
            abbrev="sigma0",
            value=1.413e-9,
            unit=u.cm ** 2,
            uncertainty=0,
            name=(
                "Resonant cross section (F4 -> F5 transition, isotropically polarized"
                " light)"
            ),
            reference="D. Steck, Cesium D Line Data, revision 1.6",
        ),
    )
    d2.f3f2_isopol = ConstantContainer(
        d=Constant(
            abbrev="d",
            value=1.310124e-29,
            unit=u.C * u.m,
            uncertainty=0,
            name="Dipole Moment (F3 -> F2 transition, isotropically polarized light)",
            reference=(
                "computed using values from D. Steck, Cesium D Line Data, revision 1.6"
            ),
        ),
        I_sat=Constant(
            abbrev="I_sat",
            value=4.63,
            unit=u.mW / u.cm ** 2,
            uncertainty=0,
            name=(
                "Saturation Intensity (F3 -> F2 transition, isotropically polarized"
                " light)"
            ),
            reference=(
                "computed using values from D. Steck, Cesium D Line Data, revision 1.6"
            ),
        ),
        sigma0=Constant(
            abbrev="sigma0",
            value=8.26e-10,
            unit=u.cm ** 2,
            uncertainty=0,
            name=(
                "Resonant cross section (F3 -> F2 transition, isotropically polarized"
                " light)"
            ),
            reference=(
                "computed using values from D. Steck, Cesium D Line Data, revision 1.6"
            ),
        ),
    )
    m = Constant(
        abbrev="mass",
        value=2.20694650e-25,
        unit=u.kg,
        uncertainty=1.7e-32,
        name="Caesium mass",
        reference="D. Steck, Cesium D Line Data, revision 1.6",
    )

    @staticmethod
    def a(B, upto="s"):
        def fbr(b0, deltab, abg):
            def a(B):
                return abg * (1 - deltab / (B - b0))

            return a

        s = fbr(-11.7 * u.G, 28.7 * u.G, 1720)
        d = fbr(47.97 * u.G, 0.12 * u.G, 1)
        g20 = fbr(19.84 * u.G, 0.005 * u.G, 1)
        g53 = fbr(53.5 * u.G, 0.0025 * u.G, 1)
        if upto == "s":
            return s(B) * a0
        if upto == "d":
            return s(B) * d(B) * a0
        if upto == "g":
            return s(B) * d(B) * g20(B) * g53(B) * a0
        return s(B) * a0

    @staticmethod
    def B(a):
        """B-field values for given a

        considers only the lowest s-type fbr
        """
        try:
            a.unit
        except (SyntaxError, AttributeError):
            a = a * a0
        B = -11.7 * u.G + 28.7 * u.G / (1 - a / (1720 * a0))
        return B


cs = Caesium()
