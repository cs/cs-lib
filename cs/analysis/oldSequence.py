import os, sys
from pathlib import Path, PosixPath
import difflib
import numpy as np
import h5py

"""
code for printing old sequence files

usage examples:

* all runs of a day: 
    folder = list_runs('2022/01/20')
* source of a run: 
    folder = '/home/jovyan/measurements/2022/01/27/it_L_scan_latt1_1'
    print_source(folder)
* parameters of a run:
    folder = '/home/jovyan/measurements/2021/12/14/C_check_fluo/'
    print_params(folder)
* diff in parameters between two runs:
    folder = list_runs('2021/12/14')
    diff_params(folder[-1], folder[0])
* diff between source of two runs:
    diff_source('/home/jovyan/measurements/2022/01/27/it_K_lunch', '/home/jovyan/measurements/2022/01/27/it_L_scan_latt2_1')

Longer example: find all folders with single plane measurements in august 2018:

import os
import glob
monthdir = '/home/jovyan/measurements/2021/08/'
days = os.listdir(monthdir)

for day in days:
    sequencefolders = [monthdir + day + '/' + folder + '/' for folder in os.listdir(monthdir + day)]
    for folder in sequencefolders:
        try:
            files = glob.glob(f"{folder}/*.hdf5")#os.listdir(folder)
            f = h5py.File(files[0])
        except IndexError:
            print(folder, 'is empty')
        except OSError:
            print('cant read file', files[0])
        try:
            splanes = f['task/parameters/sequence/single_planes/enabled'][()]
        except KeyError:
            continue
        
        if splanes:
            print(splanes, folder)

"""

def list_runs(date, sortby='date'):
    """
    return list of all subfolders in folder
    
    date:
        folder to find subfolders in. /home/jovyan/measurements prefix is optional, gets added if not present
    sortby:
        'date' or 'name', whether to sort the runs by creation date or by name. Default is date
        
    
    """
    if sortby == 'date':
        return sorted(Path('/home/jovyan/measurements').joinpath(Path(date)).iterdir(), key=os.path.getctime)
    elif sortby == 'name':
        return sorted(Path('/home/jovyan/measurements').joinpath(Path(date)).iterdir())
    else:
        raise NameError('Sortby argument can be "date" or "name"')

def print_run_source(date,run):
    """
    print the source of a run in folder date
    
    if run is an integer I, print the Ith run of the date, where the runs are sorted by creation time
    """
    if type(run) is int:
        run = list_runs(date)[run]
    
    path = Path('/home/jovyan/measurements').joinpath(Path(date), Path(run))
    print_source(path)
    
def print_source(folder):
    """
    print the source of a pys-file run folder
    """
    print('\n'.join(get_source(folder)))
    
def get_source(folder):
    """
    get the source of a pys-file run folder
    """
    with h5py.File(get_file_from_folder(folder),'r') as file:
        source = file['/task/source'][()]
    return source.decode('utf-8').split('\n')

def diff_source(folder1, folder2):
    """
    print the difference between the source of first run in folder1, and first run in folder2
    """
    f1 = get_source(folder1)
    f2 = get_source(folder2)
    
    for line in difflib.unified_diff(f1,f2, fromfile=str(folder1).split('/')[-1] , tofile=str(folder2).split('/')[-1]):
        print_diff_line(line)
    
def get_file_from_folder(folder):
    """
    extract the first hdf5 file from the folder
    """
    
    if type(folder) is not PosixPath:
        path = Path('/home/jovyan/measurements').joinpath(Path(folder))
    else:
        path = folder
    for file in path.iterdir():
        if 'expconfig' in str(file) or 'hdf5' not in str(file):
            next
        else:
            break
            
    return path.joinpath(file)
    
def load_params(folder):      
    """
    load the parameters from the first run in folder
    """
    with h5py.File(get_file_from_folder(folder),'r') as file:
        p = json.loads(file['task/parameters/parameters'][()].decode())
    return p
    
def gen_dict_str(d, shift=0):
    """
    generate a string from a dictionary, shift is the additional indent for each subdict
    """
    s = []
    for key, value in d.items():
        if type(value) is dict:
            s.append(' '*shift + f'{key}: ')
            s += gen_dict_str(value, shift+2)
        else:
            s.append(' '*shift + f'{key}: {value}')
    return s
    
class bcolors:
    """
    colors to beef up the command line prints
    """
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def print_diff_line(line):
    """
    how to markup the diff between two files (bold, underline or no markup)
    """
    if line[0] == '-':
        print(bcolors.BOLD + line + bcolors.ENDC)
    
    elif line[0] == '+':
        print(bcolors.UNDERLINE + line + bcolors.ENDC)
    
    else:
        print(line)
    
def diff_params(folder1, folder2):
    """
    print the parameters diff between the first runs in folder1 and folder2
    """
    p1 = load_params(folder1)
    p2 = load_params(folder2)
    
    for line in difflib.unified_diff(gen_dict_str(p1), gen_dict_str(p2), fromfile=str(folder1).split('/')[-1] , tofile=str(folder2).split('/')[-1]):
        print_diff_line(line)
        
def print_params(folder):
    """
    print the parameters of the first run in folder
    """
    print('\n'.join(gen_dict_str(load_params(folder))))
    
def diff_day_params(folder, ref_index=0):
    """
    print the parameter diffs between all the runs of a day and the ref_index run. 
    by default, the ref_index run is the first run (by folder creation date).
    """
    folders = list_runs(folder)
    ref_folder = folders.pop(ref_index)
    for folder in folders:
        diff_params(ref_folder, folder)
        print('#'*20)
        
        
