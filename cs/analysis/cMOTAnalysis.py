#!/usr/env/python3

import h5py
import lmfit
import numpy as np

import cs.constants as pc
import cs.units as u

__all__ = [
    "gauss",
    "offset",
    "gauss_with_offset",
    "fit_gaussian_to_OD",
    "fit_gaussian_to_series",
    "gravityModel",
    "pixel_calibration_from_TOF",
    "expansionModel",
    "find_temperature",
]

gauss = lmfit.models.GaussianModel()
offset = lmfit.models.ConstantModel()
gauss_with_offset = gauss + offset


def fit_gaussian_to_OD(OD, dim=1):
    """
    fit a 1D gaussian to the optical density image

    Parameters:
        OD : 2d array, optical density
        dim : integer, defaults to 1, dimension along which
            the OD array is summed

    Returns:
        fit.params : dictionary of fitted values
        fit.best_fit : the best fit to the data
    """
    data = OD.sum(1)
    axis = np.arange(len(data))
    init = gauss.guess(data, x=axis)
    init.add("c", data.min())  # add offset to initial paramters
    fit = gauss_with_offset.fit(data, params=init, x=axis)
    return fit.params, fit.best_fit


def fit_gaussian_to_series(ODs, dim=1):
    """
    fit 1D gaussians to a list of optical density images

    Parameters:
        ODs : list of 2d arrays, optical densities
        dim : integer, defaults to 1, dimension along which
            the OD array is summed

    Returns:
        params : a dictionary of the fitted values
            params['centers'] : list of the fitted center positions
            params['amplitudes'] : list of the fitted amplitudes
            params['sigmas'] : list of the fitted stds
            params['offsets'] : list of the fitted constant background values
    """
    params = {"centers": [], "amplitudes": [], "sigmas": [], "offsets": []}
    for OD in ODs:
        param, _ = fit_gaussian_to_OD(OD.value, dim=dim)
        params["centers"].append(param["center"])
        params["amplitudes"].append(param["amplitude"])
        params["sigmas"].append(param["sigma"])
        params["offsets"].append(param["c"])
    return params


def gravity(x, a, x0):
    return 0.5 * a * x ** 2 + x0


gravityModel = lmfit.Model(gravity)


def pixel_calibration_from_TOF(delays, centers):
    """
    compute the pixel size from a falling atomic cloud

    Parameters:
        delays : 1d-ndarray, the delays used in the
            TOF measurement in seconds
        centers : 1d-ndarray, the center positions
            of the atomic cloud in pixel

    Returns:
        pixel_size : Quantity (um/pixel), the pixel size in um/pixel
    """

    a_init = 2 * (centers[-1] - centers[0]) / (delays[-1] ** 2 - delays[0] ** 2)
    x0_init = centers[0] - 0.5 * a_init * delays[0] ** 2

    init_guess = gravityModel.make_params(a=a_init, x0=x0_init)

    f = gravityModel.fit(centers, params=init_guess, x=delays)

    return 9.81 * u.m / u.s ** 2 / (f.params["a"] * u.pixel / u.s ** 2)


def cloud_expansion(x, a, sigma0):
    return np.sqrt(a * x ** 2 + sigma0 ** 2)


expansionModel = lmfit.Model(cloud_expansion)


def find_temperature(delays, sigmas):
    """
    estimate the temperature of a falling atomic cloud

    Parameters:
        delays : 1d-ndarray, the delays used in the
            TOF measurement in seconds
        sigmas : 1d-ndarray, sizes of the atomic cloud
            in meters

    Returns:
        temperature : Quantity (K), the temperature
    """
    a_init = (sigmas[-1] ** 2 - sigmas[0] ** 2) / (delays[-1] ** 2 - delays[0] ** 2)
    sigma0_init = np.sqrt(sigmas[0] ** 2 - a_init * delays[0] ** 2)

    init_guess = expansionModel.make_params(a=a_init, sigma0=sigma0_init)

    f = expansionModel.fit(sigmas, params=init_guess, x=delays)

    return f.params["a"] * u.m ** 2 / u.s ** 2 * 133 * pc.u / pc.k_B
