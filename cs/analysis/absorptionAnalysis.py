#!/usr/env/python3

import os

import h5py
import lmfit
import numpy as np

import cs.units as u

__all__ = [
    "absorption_image",
    "atom_number_from_absorption",
    "load_file",
    "load_folder",
]


def absorption_image(f):
    """
    compute the absorption image from the h5py-file handle f

    for computation, the 2nd and 3rd image in the h5py-folder
        /data/mot/image/{1,2}/data
    are used. If a 4th image exists, the function assumes it
    is an electronic noise image (no light, no atoms) and
    substracts it from the 2nd and 3rd image before computing
    the logarithm.

    Parameters:
        f : file handle for hdf5 file, opened with h5py

    Returns:
        OD : 2D array, optical density of the atomic cloud in pixel^2
            OD = log(img3/img2)
    """
    bg = np.asarray(f["/data/mot/image/2/data"], dtype=float)
    ab = np.asarray(f["/data/mot/image/1/data"], dtype=float)
    try:  # remove electronic noise
        ref = np.asarray(f["/data/mot/image/3/data"], dtype=float)
        bg -= ref
        ab -= ref
    except:
        pass
    ab[ab < 1] = 1  # remove negative values
    bg[bg < 1] = 1
    return np.log(bg / ab) << u.pixel ** 2


def atom_number_from_absorption(OD, window=True):
    """
    using the hdf5-file handle f, compute the absorption atom number
    from column density and estimated scattering crosssection.

    The function requires the following quantities to be defined
    beforehand:
        Sigma0 : the scattering cross-section of the imaging transition
        I_img : the imaging intensity
        I_sat : the saturation intensity of the imaging transition
        Delta_img : the light's detuning from resonance
        Gamma : the linewidth of the imaging transition
        pixel_size : the size of one pixel on the camera in u.um/u.pixel

    Parameters:
        OD : 2D array, optical density of the atomic cloud in pixel^2
        window : boolean, defaults to True, if True the OD is cropped to
            the window [200:350, 300:450]

    Returns :
        estimate of atom number from absorption image
    """
    if window == True:
        OD = OD[200:350, 300:450]

    scat_cross_section = Sigma0 / (1 + I_img / Isat + 4 * Delta_img ** 2 / Gamma ** 2)
    col_density = OD / scat_cross_section
    return (np.sum(col_density) * pixel_size ** 2).si


def load_file(path, filename, window=True):
    """
    import the absorption image and estimate the atom number

    Parameters:
        path : str, path to the folder containing the file
        filename : str, filename of the file
        window : boolean, defaults to True, if True the OD is cropped to
            the window [200:350, 300:450] for the atom number estimation

    Returns:
        iter_value : float, the iter_value of the timing sequence. if
            the measurement was not part of an iteration, iter_value=-1
        OD : 2d array, optical density of the atomic cloud in pixel^2
        Natoms : estimate of the atom number from OD
    """
    with h5py.File(os.path.join(path, filename), "r") as f:
        iter_value = f["/task/iter/value"][()]
        try:
            OD = absorption_image(f)
            Natoms = atom_number_from_absorption(OD, window)
        except:
            print("Error opening file %s" % filename)
            iter_value, OD, Natoms = None, None, None
            pass
    return iter_value, OD, Natoms


def load_folder(path, window=True):
    """
    load absorption images and atom numbers from all files in a folder

    Parameters:
        path : str, path to the folder containing the files
        window : boolean, defaults to True, if True the OD is cropped to
            the window [200:350, 300:450] for the atom number estimation

    Returns:
        iter_values : list of floats, the iter_values of the timing sequence.
            if the measurement was not part of an iteration, iter_value=-1
        ODs : list of 2d arrays, optical densities of the atomic clouds in pixel^2
        Natoms : estimate of the atom numbers from OD
    """
    filenames = [f for f in os.listdir(path) if "expconfig" not in f]
    data = []
    for filename in filenames:
        iter_value, OD, Natoms = load_file(path, filename, window)
        if iter_value is None:  # if the loading failed
            continue
        data.append((iter_value, OD, Natoms))
    return zip(*data)
