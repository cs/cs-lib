import matplotlib.pyplot as plt
import numpy as np

import cs.constants as pc
import cs.units as u
from cs.atom.caesium import cs

# formulas from S. Giorgini et al., "Condensate fraction and critical temperature of a trapped interacting Bose gas", PRA, 1996


def critical_temperature(geom_avg_trap_freq, N):
    return 0.94 * pc.hbar * geom_avg_trap_freq / pc.kb * np.power(N, 1 / 3)


def finite_size_correction(geom_avg_trap_freq, mean_trap_freq, N):
    return -0.73 * mean_trap_freq / geom_avg_trap_freq * np.power(N, 1 / 3)


def interaction_correction(a, a_HO, N):
    return -1.33 * a / a_HO * np.power(N, 1 / 6)


def oscillator_length(frequency):
    return np.sqrt(np.hbat / (pc.cs.m * frequency))


def geometric_avg_trap_freq(omega_x, omega_y, omega_z):
    return np.power(omega_x * omega_y * omega_z, 1 / 3)


def mean_trap_freq(omega_x, omega_y, omega_z):
    return np.mean([omega_x, omega_y, omega_z])
