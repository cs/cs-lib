# -*- coding: utf-8 -*-

import numpy as np
import numpy.linalg as la


def normalized(v):
    """
    Normalises the given vector or array of vectors.
    """
    return v / np.expand_dims(la.norm(v, axis=-1), -1)


normalised = normalized


def get_cos_angle(a, b, c):
    """
    Computes the cosine of the angle between the three given points or
    arrays of points. The angle is computed for the distances AB and BC,
    i.e. B is the point with angle.
    """
    v1 = normalized(a - b)
    v2 = normalized(c - b)
    return np.inner(v1, v2)


def base_vectors(n):
    """
    Returns 3 orthogonal base vertors, the first one colinear to n.

    Parameters
    ----------
        n: ndarray, shape (3, )
            A vector giving direction of the basis

    Returns
    -------
        n: ndarray, shape (3, )
            The first vector of the basis
        l: ndarray, shape (3, )
            The second vector of the basis
        m: ndarray, shape (3, )
            The first vector of the basis

    """
    n = normalized(n)

    # choose two vectors perpendicular to n
    # arbitrary choice due to symmetry
    if np.abs(n[0]) == 1:
        l = np.r_[n[2], 0, -n[0]]
    else:
        l = np.r_[0, n[2], -n[1]]
    l = normalized(l)
    m = np.cross(n, l)
    return n, l, m


def ssc(v):
    """
    Returns the skew-symmetric cross-product matrix of v. This can be used
    for cross product computation:
    cross(v, u) == dot(ssc(v), u)
    """
    return np.array(
        [[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]], dtype=v.dtype
    )


def rotation_from_vectors(a, b):
    """
    Computes the rotation matrix which transforms the reference vector a
    into the target vector b.
    """
    # computation relies on unit vectors
    b2 = normalized(a)
    a = normalized(b)
    b = b2

    assert a.shape == (3,)
    assert a.shape == b.shape, "input vectors must be of sapme shape"

    # the computation below does not work for opposite vectors
    if np.equal(a, -b).all():
        return -1 * np.eye(3)

    v = np.cross(a, b)
    sscab = ssc(v)
    return np.identity(3) + sscab + la.matrix_power(sscab, 2) / (1 + np.dot(a, b))


def in_translated_rotated_reference_frame(
    position, normal, coords, fun, *args, **kwargs
):
    """
    Transforms all coordinates specified by coords according to the position
    and normal vectors. First all coordinates are translated by the position
    vector. Then they are rotated to align the normal vector to the z-axis.
    Then the function is applied and the result is transformed back into the
    original frame.
    """
    coords = np.asanyarray(coords)
    normal = np.asanyarray(normal)
    position = np.asanyarray(position)
    assert normal.shape == (3,)
    assert position.shape == (3,)
    assert coords.shape[0] == 3, str(coords.shape)
    assert np.linalg.norm(normal) != 0
    rot_m = rotation_from_vectors(normal, [0, 0, 1])

    r = ((coords.T - position) @ rot_m).T

    res = np.asanyarray(fun(r, *args, **kwargs))

    return (res.T @ np.linalg.inv(rot_m)).T


def norm(v, *args, **kwargs):
    return np.linalg.norm(v.si, *args, **kwargs) * v.unit


def cross(a, b, *args, **kwargs):
    return np.cross(a.si, b.si, *args, **kwargs) * a.unit * b.unit


def tensordot(a, b, *args, **kwargs):
    return np.tensordot(a.si, b.si, *args, **kwargs) * a.unit * b.unit
