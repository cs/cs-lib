# -*- coding: utf-8 -*-
"""
@author Hendrik v. Raven

Export unit definitions to be used in all other caesium projects.

Currently this is nothing more than a reexport of the ufloat library.
"""

import astropy.units as u
import attr
from decorator import decorator
from toolz.functoolz import compose


@decorator
def SI(f, *args, **kwargs):
    temp = f(*args, **kwargs)
    return temp.si


# composed version of SI as the ordering in combination with property is
# important.
SIproperty = compose(property, SI)


def unit_attr(unit, *args, **kwargs):
    from astropy.units import Quantity

    return attr.ib(*args, converter=lambda x: Quantity(x).to(unit), type=unit, **kwargs)


def unit_value(unit, *args, **kwargs):
    """
    attr.ib extension checking the unit and converting to a value without unit afterwards
    """
    from astropy.units import Quantity

    return attr.ib(
        *args, converter=lambda x: Quantity(x).to(unit).value, type=unit, **kwargs
    )


from astropy.units import *  # isort:skip
import astropy.units as u  # isort:skip
from astropy.units import imperial  # isort:skip

imperial.enable()
