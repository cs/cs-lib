"""
Generic algorithms not existing in other packages for usage with numpy arrays
"""
import collections

import numpy as np


def piecewise(x, funclist, condlist, shape=(), *args, **kwargs):
    """
    This is a more generic implementation of a piecewise function following
    np.piecewise.  np.piecewise only supports functions returning scalar
    values, we want to return 3x3 matrices.
    This does only work in python 3.5+ as it requires PEP 448.
    """
    x = np.asanyarray(x)
    condlist = np.array(condlist, dtype=bool)

    y = np.zeros((*x.shape, *shape), x.dtype)
    for k in range(len(condlist)):
        item = funclist[k]

        if not isinstance(item, collections.abc.Callable):
            y[condlist[k]] = item
        else:
            vals = x[condlist[k]]
            if vals.size > 0:
                y[condlist[k]] = item(vals, *args, **kwargs)

    return y


def equivalence_partition_nohash(iterable, relation):
    classes = []
    for item in iterable:
        for c in classes:
            if relation(c[0], item):
                c.append(item)
                break
        else:
            classes.append([item])
    return classes


def minmax(arr):
    """map array into [0,1]"""
    arr -= arr.min()
    return arr / arr.max()


def norm(arr):
    """divide array by arr.max()"""
    return arr / arr.max()


def crop(arr, center, size):
    """crop array around center with sidelength 2*size"""
    return arr[
        max(center[0] - size, 0) : center[0] + size + 1,
        max(center[1] - size, 0) : center[1] + size + 1,
    ]


def fast2dPeakFinder2(img, threshold=None, gsigma=1, maxfilter=3):
    """
    Adaptation of
      fast2dPeakFinder
      From
       Natan (2021). Fast 2D peak finder (https://www.mathworks.com/matlabcentral/fileexchange/37388-fast-2d-peak-finder), MATLAB Central File Exchange. Retrieved August 21, 2021.

    Uses scipy.ndimage.maximum_filter to remove local maximum test in fast2dPeakFinder.

    img: image
    threshold: threshold to remove background
    gsigma: gaussian filter sigma width
    maxfilter: maximum filter size

    returns:
      centers: the centers of the local maxima
    """
    if threshold is None:
        # min(max(arr,dim)) gives the smallest maximum along dim
        # take max of smallest maxima along possible dimensions for threshold
        #  -> highest background pixel value in a row where there is no fluorescence
        threshold = max([np.min(np.max(img, 0)), np.min(np.max(img, 1))])

    img_ = median_filter(img, 3)
    img_[img_ < threshold] = 0
    img_ = gaussian_filter(img_, gsigma)
    img_[img_ < 0.9 * threshold] = 0

    img_m = maximum_filter(img_, maxfilter)

    xx, yy = np.where(np.logical_and(img_ == img_m, img_ != 0))  # might have to flipped

    return xx, yy
