#
#   CLASS TO SIMULATE FLUORESCENCE IMAGES
#
#
#
#
#   Changelog:
#   - 2021/09/09: Added option to vary intensity of individual atoms randomly with some spread (parameter: intensity_deviation)
#   - 2021/09/12: Added option to include a border region of unfilled sites
#   - 2021/09/13: Fixed bug in site centers computation, variable cam_scale_factor was inexact
#

import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import RectBivariateSpline
from scipy.ndimage import affine_transform, map_coordinates
from scipy.signal import convolve2d
from scipy.special import jv
from scipy.stats import norm, poisson

default_dict = {
    "sites": 30,
    "lattice_a": 767 / 2,
    "occupation_prop": 0.5,
    "super_sampling": 5,
    "psf_size": 760,
    "photonNumber": 1e3,
    "angle": 0,
    "ox": 0,
    "oy": 0,
    "pixelSize": 13e3,
    "magnification": 60,
    "pixel_super_sampling": 5,
    "readoutNoise": 5,
    "intensity_deviation": 0.0,
    "empty_border_width": 0,
}


class latticeContainerClass:
    """
    simulate what a fluorescence image would look like.

    To generate an image, use the class methods
      fromDict(paramsDict, withnoise=True)
    or
      default(withnoise=True)
    or
      from_site_config(siteconfig, paramDict={}, psf = None, withnoise=True)
       where siteconfig is the lattice occupation. psf allows insertion of
       an external PSF.

    The class produces the image by randomly filling a lattice at
    a specified occupation. This image is supersampled, and at each
    atom position the PSF is inserted. Afterwards the lattice
    is translated and rotated. In the next step the real lattice
    is mapped to the camera pixels by spline interpolation. For
    spline interpolation, a supersampled camera chip is used to
    reduce interpolation errors. After down sampling to the correct
    camera chip size, the readout noise is added.

    the paramsDict for the default class method is:
     lattice: 30 sites, 767/2 nm spacing, half filling
     PSF: 760 nm airy radius, 1e3 phtons per atoms
     camera: 13 um px size, x60 magnification, 5 counts/px readout noise

    if withnoise is true, a photon number is drawn from
    a poisson distribution of the given mean photon number and the
    PSF is sampled that number of times. In addtion, read out
    noise is added to the camera image.

    Arguments for paramDict:
     sites: 30, int, number of sites of the lattice
     lattice_a: 767/2, float, lattice constant
     occupation_prop: .5, float, occupation probability
     super_sampling: 5, int, pixel per lattice site of the real lattice
     lattice: None, ndarray(sites,sites), lattice occupation matrix to use
       instead of a newly generated one
     empty_border_width: 0, int, padding at the edges around the lattice
       to avoid edge effects
     psf_size: 760, float, PSF size in nm
     photonNumber: 1e3, float, mean photon number per atom
     intensity_deviation: 0, float, intensity devitation of each psf.
       normal distributed with center=1 and std=intensity_deviation.
       if intensity_deviation==0, this step is skipped.
     angle: 0, float, angle in radian by which to rotate the lattice
     ox,oy: 0, float, offsets of the lattice in x and y direction in
       lattice_a/super_sampling
     pixelSize: 13e3, float, pixel size of the camera in nm
     magnification: 60, float, magnification of the imaging system
     pixel_super_sampling: 5, int, super sampling of the camera chip
       (used in interpolation to map real lattice to camera lattice)
     readoutNoise: 10, int, mean readout noise per pixel, assumed to be
       normal distributed

    class attributes:
     self.lattice: the occupation matrix
     self.super_sampling: the super sampling of the real lattice
     self.dx: the sampling in nm of the real lattice
     self.lattice_real: the real lattice (i.e. super sampled but w/o
       PSF added)
     self.sites: the number of sites in the lattice
     self.psf_x: the axis of the PSF
     self.psf: the PSF
     self.image: the real lattice convolved with the (possibly noisy) PSF
       and translated and rotated
     self.camImage: the image taken by the camera, possibly with added
       readout noise
     self.cam_lattice_real: the super sampled image on the camera
     self.cam_scale_factor: scale factor to convert real lattice distances
       to camera lattice distances
     self.centers: the lattice site centers on the camera image assuming no
       rotation or translation of the lattice
    """

    def __init__(self):
        pass

    @classmethod
    def fromDict(cls, paramDict={}, withnoise=True):
        """
        usage:
         paramDict is a dict of arguments to pass to any
         of the class methods and their value.

        example:
         fromDict(paramDict=dict(sites=50, occupation_prop=.1))
         generates a simulated image of 50 sites with occupation
         propability of 10 %.

        returns:
         an instance of latticeContainerClass
        """
        latt = cls()
        latt.genLatticeOccupation(**paramDict)
        latt.genPSF(**paramDict)
        if withnoise:
            latt.addPhotonShotNoise(**paramDict)
        else:
            latt.convoluteWithPSF()
        latt.translateAndRotate(**paramDict)
        latt.toCameraPixels(**paramDict)
        latt.addReadoutNoise(**paramDict)
        latt.get_centers(**paramDict)
        return latt

    @classmethod
    def default(cls, withnoise=True):
        latt = cls()
        latt.genLatticeOccupation()
        latt.genPSF()
        if withnoise:
            latt.addPhotonShotNoise()
        else:
            latt.convoluteWithPSF()
        latt.translateAndRotate()
        latt.toCameraPixels()
        latt.addReadoutNoise()
        return latt

    @classmethod
    def from_site_config(cls, siteconfig, paramDict={}, psf=None, withnoise=True):
        latt = cls()

        latt.lattice = siteconfig
        latt.calc_real_lattice(**paramDict)
        if psf is None:
            latt.genPSF(**paramDict)
        else:
            latt.psf = psf
        if withnoise:
            latt.addPhotonShotNoise(**paramDict)
        else:
            latt.convoluteWithPSF()
        latt.translateAndRotate(**paramDict)
        latt.toCameraPixels(**paramDict)
        latt.addReadoutNoise(**paramDict)
        latt.get_centers(**paramDict)
        return latt

    def calc_real_lattice(
        self,
        sites=30,
        lattice_a=767 / 2,
        occupation_prop=0.5,
        super_sampling=5,
        **kwargs,
    ):

        dx = lattice_a / super_sampling
        lattice_real = np.zeros((sites * super_sampling, sites * super_sampling))
        lattice_real[
            super_sampling // 2 :: super_sampling, super_sampling // 2 :: super_sampling
        ] = self.lattice

        self.super_sampling = super_sampling
        self.dx = dx
        self.lattice_real = lattice_real
        self.sites = sites

    def genLatticeOccupation(
        self,
        sites=30,
        lattice_a=767 / 2,
        occupation_prop=0.5,
        super_sampling=5,
        empty_border_width=0,
        lattice=None,
        **kwargs,
    ):
        if lattice is None:
            lattice = np.random.random(size=(sites, sites))
        lattice = lattice < occupation_prop
        lattice = np.pad(lattice, int(empty_border_width))

        dx = lattice_a / super_sampling
        lattice_real = np.zeros(
            (
                (sites + 2 * int(empty_border_width)) * super_sampling,
                (sites + 2 * int(empty_border_width)) * super_sampling,
            )
        )
        lattice_real[
            super_sampling // 2 :: super_sampling, super_sampling // 2 :: super_sampling
        ] = lattice

        self.lattice = lattice
        self.super_sampling = super_sampling
        self.dx = dx
        self.lattice_real = lattice_real
        self.sites = sites + 2 * int(empty_border_width)

    def genPSF(self, psf_size=760, **kwargs):
        dx = self.dx
        psf_x = np.arange(-(4 * psf_size) // dx, (4 * psf_size) // dx) * dx
        x = 1.22 * psf_x / psf_size * np.pi
        x, y = np.meshgrid(x, x)
        r = np.sqrt(x ** 2 + y ** 2)
        psf = (2 * jv(1, r) / r) ** 2
        psf[r == 0] = 1

        self.psf_x = psf_x
        self.psf = psf

    def addPhotonShotNoise(self, photonNumber=1e3, intensity_deviation=0.0, **kwargs):

        int_dev = intensity_deviation

        ii = np.arange(self.psf.size)
        lattice_noisy = np.zeros(self.lattice_real.shape)
        i0 = self.super_sampling // 2 - 1
        dsx, dsy = self.psf.shape
        lx, ly = self.lattice_real.shape

        for xsite in range(self.sites):
            for ysite in range(self.sites):

                if not self.lattice[xsite, ysite]:
                    continue

                if int_dev > 0.0:
                    n_phot_cur = np.random.normal(1, int_dev) * photonNumber
                else:
                    n_phot_cur = photonNumber
                photon_pos = np.random.choice(
                    ii,
                    size=poisson.rvs(n_phot_cur),
                    p=self.psf.reshape(-1) / np.sum(self.psf),
                )
                distr = np.zeros(self.psf.shape)
                unique, counts = np.unique(photon_pos, return_counts=True)
                distr.flat[unique] = counts

                ix = i0 + self.super_sampling * xsite
                iy = i0 + self.super_sampling * ysite

                nxm = ix if ix - dsx // 2 < 0 else dsx // 2
                nxp = lx - ix if lx < ix + dsx // 2 else dsx // 2
                nym = iy if iy - dsy // 2 < 0 else dsy // 2
                nyp = ly - iy if ly < iy + dsy // 2 else dsy // 2

                lattice_noisy[ix - nxm : ix + nxp, iy - nym : iy + nyp] += distr[
                    dsx // 2 - nxm : dsx // 2 + nxp, dsy // 2 - nym : dsy // 2 + nyp
                ]

        self.image = lattice_noisy

    def convoluteWithPSF(self, **kwargs):
        self.image = convolve2d(self.lattice_real, self.psf, mode="same")

    def translateAndRotate(self, angle=0, ox=0, oy=0, **kwargs):
        centre = 0.5 * np.array(self.image.shape)
        rot = np.array(
            [[np.cos(angle), np.sin(angle)], [-np.sin(angle), np.cos(angle)]]
        )
        offset = (centre - centre.dot(rot)).dot(np.linalg.inv(rot)) + np.array((ox, oy))
        self.image = affine_transform(self.image, rot, offset=-offset)

    def toCameraPixels(
        self, pixelSize=13e3, magnification=60, pixel_super_sampling=5, **kwargs
    ):
        pixelSize = pixelSize / magnification
        dx_cam = pixelSize / pixel_super_sampling
        print(self.dx, dx_cam)
        # supersample to camera pixel grid
        x_image = np.arange(self.image.shape[0]) * self.dx
        x_cam = np.arange((self.image.shape[0] * self.dx) // dx_cam) * dx_cam

        spline_func = RectBivariateSpline(x_image, x_image, self.image)
        spline_func_latt = RectBivariateSpline(x_image, x_image, self.lattice_real)
        camimg = spline_func(x_cam.reshape(-1), x_cam.reshape(-1))
        cam_lattice_real = spline_func_latt(x_cam.reshape(-1), x_cam.reshape(-1))

        # integrate each pixel for camera image
        s1 = camimg.shape[0] // pixel_super_sampling
        c = (
            camimg[: pixel_super_sampling * s1, : pixel_super_sampling * s1]
            .reshape(s1, pixel_super_sampling, s1, pixel_super_sampling)
            .sum((1, 3))
        )
        c_latt = (
            cam_lattice_real[: pixel_super_sampling * s1, : pixel_super_sampling * s1]
            .reshape(s1, pixel_super_sampling, s1, pixel_super_sampling)
            .sum((1, 3))
        )

        orig_integral = self.image.sum()  # *self.dx**2
        new_integral = c.sum()  # *dx_cam**2
        if new_integral != 0.0:
            self.camImage = c * orig_integral / new_integral
            self.cam_lattice_real = c_latt * orig_integral / new_integral
        else:
            self.camImage = c
            self.cam_lattice_real = c_latt

        self.cam_scale_factor = self.dx / (
            pixel_super_sampling * dx_cam
        )  # len(self.camImage)/len(self.image) #

    def get_centers(
        self,
        sites=30,
        pixelSize=13e3,
        magnification=60,
        pixel_super_sampling=5,
        **kwargs,
    ):
        cs = [
            [
                (ix * pixel_super_sampling + pixel_super_sampling / 2)
                * self.cam_scale_factor,
                (iy * pixel_super_sampling + pixel_super_sampling / 2)
                * self.cam_scale_factor,
            ]
            for ix in np.arange(0, self.sites)
            for iy in np.arange(0, self.sites)
        ]

        self.centers = np.array(cs)

    def addReadoutNoise(self, readoutNoise=10, **kwargs):
        readout_noise = norm.rvs(
            loc=0, scale=readoutNoise, size=self.camImage.size
        ).reshape(self.camImage.shape)
        self.camImage += readout_noise
