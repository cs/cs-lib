# -*- coding: utf-8 -*-
"""
Created on Tue May 09 21:33:06 2017

@author: tillk_000
"""

import cs.atom.classes as ac
from cs.units import C, GHz, Hz, MHz, THz, kg, m

caesium = ac.atom(2.20694650e-25 * kg)

# s1/2 manifold
caesium.addLevel(ac.line(caesium, "2s1/2f3", 0 * Hz, -0.35 * MHz))
caesium.addLevel(ac.line(caesium, "2s1/2f4", 9.192631770 * GHz, 0.35 * MHz))

d1frequency = 335.116048807 * THz
# p1/2 manifold
caesium.addLevel(
    ac.line(
        caesium,
        "2p1/2f3",
        5.170855370625 * GHz + d1frequency - 656.820 * MHz,
        -0.12 * MHz,
    )
)
caesium.addLevel(
    ac.line(
        caesium,
        "2p1/2f4",
        5.170855370625 * GHz + d1frequency - 510.860 * MHz,
        0.12 * MHz,
    )
)

d2frequency = 351.72571850 * THz
# p3/2 manifold
caesium.addLevel(
    ac.line(
        caesium,
        "2p3/2f2",
        5.170855370625 * GHz + d2frequency - 339.64 * MHz,
        -0.93 * MHz,
    )
)
caesium.addLevel(
    ac.line(
        caesium, "2p3/2f3", 5.170855370625 * GHz + d2frequency - 188.44 * MHz, 0 * Hz
    )
)
caesium.addLevel(
    ac.line(
        caesium,
        "2p3/2f4",
        5.170855370625 * GHz + d2frequency + 12.815 * MHz,
        0.37 * MHz,
    )
)
caesium.addLevel(
    ac.line(
        caesium,
        "2p3/2f5",
        5.170855370625 * GHz + d2frequency + 263.81 * MHz,
        0.56 * MHz,
    )
)

# d2 lines
d2Coupling = 3.7971e-29 * C * m
d2lineWidth = 5.2227 * MHz
caesium.addTransition("2s1/2f3", "2p3/2f2", d2Coupling, d2lineWidth)
caesium.addTransition("2s1/2f3", "2p3/2f3", d2Coupling, d2lineWidth)
caesium.addTransition("2s1/2f3", "2p3/2f4", d2Coupling, d2lineWidth)
caesium.addTransition("2s1/2f3", "2p3/2f5", d2Coupling, d2lineWidth)
caesium.addTransition("2s1/2f4", "2p3/2f2", d2Coupling, d2lineWidth)
caesium.addTransition("2s1/2f4", "2p3/2f3", d2Coupling, d2lineWidth)
caesium.addTransition("2s1/2f4", "2p3/2f4", d2Coupling, d2lineWidth)
caesium.addTransition("2s1/2f4", "2p3/2f5", d2Coupling, d2lineWidth)

# d1 lines
d1Coupling = 2.6980e-29 * C * m
d1lineWidth = 4.5612 * MHz
caesium.addTransition("2s1/2f3", "2p1/2f3", d1Coupling, d1lineWidth)
caesium.addTransition("2s1/2f3", "2p1/2f4", d1Coupling, d1lineWidth)
caesium.addTransition("2s1/2f4", "2p1/2f3", d1Coupling, d1lineWidth)
caesium.addTransition("2s1/2f4", "2p1/2f4", d1Coupling, d1lineWidth)

# higher order levels. here the labelling i have chosen kinda breaks down. maybe
# adjust level class such that if first two values in label are numeric, then the
# first value is n and the second s

##6 2d3/2 manifold
#Value found for transition frequency: 4.34669e14
caesium.addLevel(ac.line(caesium, '62d3/2f2',6.7734401e14*Hz))
caesium.addLevel(ac.line(caesium, '62d3/2f3',6.7734401e14*Hz))
caesium.addLevel(ac.line(caesium, '62d3/2f4',6.7734401e14*Hz))
caesium.addLevel(ac.line(caesium, '62d3/2f5',6.7734401e14*Hz))

#d32Coupling = 

##6 2d5/2 manifold
#Value found for transition frequency: 4.37577e14
caesium.addLevel(ac.line(caesium, '62d5/2f1',6.7872415e14*Hz))
caesium.addLevel(ac.line(caesium, '62d5/2f2',6.7872415e14*Hz))
caesium.addLevel(ac.line(caesium, '62d5/2f3',6.7872415e14*Hz))
caesium.addLevel(ac.line(caesium, '62d5/2f4',6.7872415e14*Hz))
caesium.addLevel(ac.line(caesium, '62d5/2f5',6.7872415e14*Hz))
caesium.addLevel(ac.line(caesium, '62d5/2f6',6.7872415e14*Hz))

#d52Coupling =
#d52lineWidth =  Hz 
#
##7 2s1/2 manifold
caesium.addLevel(ac.line(caesium, '72s1/2f3',5.5568117e14))
caesium.addLevel(ac.line(caesium, '72s1/2f4',5.5568117e14))
##7 2p1/2 manifold
b1frequency = 6.5250865e14 *Hz
b1lineWidth = 0.1263 * MHz
caesium.addLevel(ac.line(caesium, '72p1/2f3',6.5250865e14*Hz))
caesium.addLevel(ac.line(caesium, '72p1/2f4',6.5250865e14*Hz))

##7 2p3/2 manifold
b2frequency = 6.5787241e14 *Hz
b2lineWidth = 0.292 * MHz  ##
caesium.addLevel(ac.line(caesium, '72p3/2f2',6.5794142e14*Hz)) #https://doi.org/10.1088/1612-202X/aac97e
caesium.addLevel(ac.line(caesium, '72p3/2f3',6.5794147e14*Hz))
caesium.addLevel(ac.line(caesium, '72p3/2f4',6.5794154e14*Hz))
caesium.addLevel(ac.line(caesium, '72p3/2f5',6.5794162e14*Hz))

b1Coupling = 2.3578301e-30 *C *m 
caesium.addTransition("2s1/2f3", "72p1/2f3", b1Coupling, b1lineWidth)
caesium.addTransition("2s1/2f3", "72p1/2f4", b1Coupling, b1lineWidth)
caesium.addTransition("2s1/2f4", "72p1/2f3", b1Coupling, b1lineWidth)
caesium.addTransition("2s1/2f4", "72p1/2f4", b1Coupling, b1lineWidth)

b2Coupling = 4.8680163e-30 * C * m ##https://arxiv.org/pdf/1904.06362.pdf
caesium.addTransition("2s1/2f3", "72p3/2f2", b2Coupling, b2lineWidth)
caesium.addTransition("2s1/2f3", "72p3/2f3", b2Coupling, b2lineWidth)
caesium.addTransition("2s1/2f3", "72p3/2f4", b2Coupling, b2lineWidth)
caesium.addTransition("2s1/2f3", "72p3/2f5", b2Coupling, b2lineWidth)
caesium.addTransition("2s1/2f4", "72p3/2f2", b2Coupling, b2lineWidth)
caesium.addTransition("2s1/2f4", "72p3/2f3", b2Coupling, b2lineWidth)
caesium.addTransition("2s1/2f4", "72p3/2f4", b2Coupling, b2lineWidth)
caesium.addTransition("2s1/2f4", "72p3/2f5", b2Coupling, b2lineWidth)

caesium.makeLtrans()

cesium = caesium
cs = caesium
