import numpy as np
import scipy.constants as pc


class opticalLattice(object):
    def __init__(self, line, atom, laserBeam, NlatticeSites=32, NSampling=32, B=0):
        self.line = line
        self.atom = atom
        self.laserBeam = laserBeam
        self.B = B
        if self.mf is None:
            self.mf = 0
        self._period = self.wavelength / 2
        self._NlatticeSites = NlatticeSites
        self.NSampling = NSampling
        self.ascatt = 0

    @property
    def laserBeam(self):
        return self._laserBeam

    @laserBeam.setter
    def laserBeam(self, value):
        assert isinstance(value, "laserBeam")
        self._laserBeam = value

    @property
    def wavelength(self):
        return self._laserBeam.l

    @wavelength.setter
    def wavelength(self, value):
        self._laserBeam.l = value

    @property
    def polarization(self):
        return self._laserBeam.pol

    @polarization.setter
    def polarization(self, value):
        assert value in [-1, 0, 1]
        self._laserBeam.pol = value

    @property
    def mf(self):
        return self.line.mf

    @property
    def period(self):
        return self._period

    @period.setter
    def period(self, value):
        self._period = value
        self.reinit()

    @property
    def NlatticeSites(self):
        return self._NlatticeSites

    @NlatticeSites.setter
    def NlatticeSites(self, value):
        self._NlatticeSites = value
        self.reinit()

    @property
    def NSampling(self):
        return self._NSampling

    @NSampling.setter
    def NSampling(self, value):
        self._NSampling = value
        self.reinit()

    @property
    def V0(self):
        return self.line.lightshift(self.laserBeam, mfinit=self.mf, B=self.B)

    @property
    def trapFreq(self):
        return np.sqrt(4 * abs(self.V0) / pc.hbar ** 2 * self.Er)

    @property
    def Er(self):
        return self.wavelength ** 2 * pc.h ** 2 / 2 / self.atom.m

    @property
    def U(self):
        return (
            pc.h ** 2
            / np.pi
            / self.atom.m
            * self.ascatt
            * (np.sum(np.abs(self.wannier) ** 4, axis=0) * self.period / self.NSampling)
            ** 3
        )

    @property
    def J(self):
        return 0.25 * (np.max(self.bands, axis=0) - np.min(self.bands, axis=0))

    def computeBandstructure(self):
        bands = np.zeros((self.NlatticeSites, self.NSampling))
        u = np.zeros((self.NlatticeSites, self.NSampling, self.NSampling))

        for ind, q in enumerate(self.q):
            h = np.diag((self.k - q) ** 2)
            v = (
                0.25
                * self.V0
                * (np.eye(self.NSampling, k=-1) + np.eye(self.NSampling, k=1))
            )

            e, w = np.linalg.eig(h + v)
            ii = np.argsort(e)
            e = e[ii]
            w = w[:, ii]

            ux = np.fft.ifftshift(np.fft.fft(np.fft.fftshift(w)))
            ux = self.normalize(ux, self.NlatticeSites / self.period / self.NSampling)
            ux[:, np.where(np.mean(ux, axis=0) > 0)] *= -1

            bands[ind] = e
            u[ind] = ux

        return bands, u

    def computeWannierFunctions(self):
        ux = np.tile(self.ux, (1, self.NlatticeSites, 1))
        wannier = np.zeros(ux.shape) + 0j

        for ind, xs in enumerate(self.sitePos):
            tmp = np.sum(
                ux
                * np.transpose(
                    np.tile(
                        np.exp(1j * np.outer(self.q, self.x - xs)),
                        (self.NSampling, 1, 1),
                    ),
                    (1, 2, 0),
                ),
                axis=0,
            )
            wannier[ind] = self.normalize(tmp, self.period / self.NSampling)
        return wannier

    def reinit(self):
        try:
            self.k = self.fftaxis(self.NSampling, self.period / self.NSampling)
            self.q = self.fftaxis(self.NlatticeSites, self.period)
            self.x = (
                np.arange(
                    -(self.NlatticeSites + 1) * self.NSampling / 2,
                    (self.NlatticeSites - 1) * self.NSampling / 2,
                )
                * self.period
                / self.NSampling
            )
            self.sitePos = (
                np.arange(self.NlatticeSites / 2, self.NlatticeSites / 2) * self.period
            )
            self.bands, self.ux = self.computeBandstructure()
            self.wannier = self.computeWannierFunctions()
        except:
            pass

    @staticmethod
    def fftaxis(n, *args):
        if not args:
            return 2 * np.pi * np.fft.fftshift(np.fft.fftfreq(len(n), n[1] - n[0]))
        return 2 * np.pi * np.fft.fftshift(np.fft.fftfreq(n, args[0]))

    @staticmethod
    def normalize(func, dx):
        return func / np.sqrt(np.sum(np.abs(func) ** 2) * dx)
