# -*- coding: utf-8 -*-
"""
TODO When setting mf, it is set for all variables pointing to
    the same line. Problematic when trying to compute e.g.
    Raman transition between states of the same hyperfine
    manifold. I have not figured out a good way to solve this
    issue, the hack that is used is to always set mf in the 
    calling function e.g. 
        ramanTransition(..., mfinit=1, mffinal=0)
    instead of just ramanTransition(...) (which I feel would 
    be nicer) 
"""
import itertools
from typing import Optional

import attr
import numpy as np
import scipy.constants as pc
import sympy.physics.quantum.cg as cg
from numpy import pi, sqrt
from sympy.physics.wigner import clebsch_gordan, wigner_6j

from cs.algorithms import equivalence_partition_nohash
from cs.constants import c, eps0, h, hbar, muB
import cs.units as u
from cs.units import C, Hz, J, Quantity, kg, m, quantity_input, s


class line:
    @quantity_input
    def __init__(self, element, label, wres, gf=None):
        """
        element : the element to which this line belongs, an
          instance of the atom class
        label   : a string of the form '2s2/3f1' following the
          standard notation for quantum numbers
        wres    : the level energy
        gf      : is the gf values of the line in Hz/Gauss
        """
        label = label.lower()
        self.label = label
        self.Ldict = {"s": 0, "p": 1, "d": 2}
        self.n =6
        if (
            ord(label[1]) <= 57
        ):  # if the first two indices are numbers, then label specifies n as well
            self.n = int(label[0])
            label = label[1:]
        self.S = int(label[0])

        self.L = self.Ldict.get(label[1], ord(label[1]) - ord("d") + 2)

        self.J = int(label[2]) * 0.5
        self.F = int(label[6])
        self._mf = None
        self.gf = gf if gf is not None else 0  # if gf is not given, set gf=0

        self.res = wres
        self.element = element

    @property
    def mf(self):
        """the magnetic quantum number of the line

        Note: this property is problematic, since usually we don't use
            copies of lines associated with an atom. This means that
            when setting mf, it is set for all variables pointing to
            the same line. Problematic when trying to compute e.g.
            Raman transition between states of the same hyperfine
            manifold. I have not figured out a good way to solve this
            issue, the hack that is used is to always set mf in the
            calling function e.g.
                ramanTransition(..., mfinit=1, mffinal=0)
            instead of just ramanTransition(...) (which I feel would
            be nicer)"""
        return self._mf

    @mf.setter
    def mf(self, value):
        if abs(value) <= self.F:
            self._mf = value
        else:
            raise ValueError("mF out of range")

    def getquNum(self):
        """get the quantum numbers of the line"""
        return (self.S, self.L, self.J, self.F, self.mf)

    def findTransitions(self, element=None):
        """find the transitions from this line.

        Note: the argument element is not used, its currently included
        for legacy purposes (read:I was too lazy to find all instances
        where this was used).
        """
        return [t for t in self.element.transitions if t.hasLevel(self.label)]

    def resonance(self, B=0):
        """
        get the resonance frequency if there is a magnetic field. B is in Gauss
        """
        if self.mf:
            return self.res + self.mf * self.gf * B
        else:
            return self.res

    def getManifoldLabel(self):
        return (self.n, self.L, self.J)

    def __str__(self):
        return self.label + ("mf%s" % self.mf if self.mf else "")

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return (
                self.label == other.label
            )  # if the other is not just a label but a level
        elif isinstance(other, "a".__class__):
            return self.label == other.lower()  # if the other is just a label
        return False

    def __ne__(self, other):
        return not self.__eq__(other)


class transition:
    @quantity_input
    def __init__(self, initline, finalline, coupling: C * m, linewidth: Hz, I):
        """instantiate new transition

        Args:
            initline : instance of cs.atom.classes.line
                initial state of the transition. Really just one of
                the two states connected by this transition, but
                ideally the one with the lower energy
            finalline : instance of cs.atom.classes.line
                final state of the transition. Really just one of
                the two states connected by this transition, but
                ideally the one with the higher energy
            coupling : astropy.Quantity
                the <J|| er ||J'> matrix element.
            linewidth : astropy.Quantity
                the linewidth of the transition (this could be
                calculated from the coupling, but its not)
            I : float
                the nuclear spin
        """
        self.initial = initline
        self.final = finalline
        self.coupling = coupling * self.racah(
            initline.J, finalline.J, initline.F, finalline.F, I
        )
        self.linewidth = linewidth

    def dipole(self, polarisation, mfinit=None, mffinal=None) -> C * m:
        """compute the dipole matrix element of the transition

        Args:
            polarisation: int
                the polarisation of the coupling light. The
                polarisation needs to be in [-1, 0, 1] corresponding
                to [s+, pi, s-] polarized light.
            mfinit: int
                the magnetic quantum number of the inital (usually
                lower energy) state. check self.initial to see what
                the initial line of this transition is.
            mffinal: int
                the magnetic quantum number of the final (usually
                higher energy) state. check self.final to see what
                the final line of this transition is.

        Returns:
            mu : astropy.Quantity
                the dipole matrix elements of the transition. Unit: Cm
        """
        if mfinit is not None:
            try:
                self.initial.mf = mfinit
            except:
                return 0 * C * m
                # raise ValueError('initial mf out of range  mfi=%d'%mfinit)
        if mffinal is not None:
            try:
                self.final.mf = mffinal
            except:
                return 0 * C * m
                # raise ValueError('Final mf in resonance computation out of range. mff=%d'%mffinal)
        assert self.initial.mf is not None, "undefined initial mf"
        assert self.final.mf is not None, "undefinde final mf"
        assert abs(polarisation) <= 1, "polarisation out of range"
        return self.coupling * float(
            clebsch_gordan(
                self.final.F,
                1,
                self.initial.F,
                self.final.mf,
                polarisation,
                self.initial.mf,
            )
        )

    def sharesLevelWith(self, trans):
        """True if this transition shares a level with transition trans"""
        return (
            (self.initial == trans.inital)
            or (self.initial == trans.final)
            or (self.final == trans.inital)
            or (self.final == trans.final)
        )

    def hasLevel(self, level):
        """True if this transition is from or to line level"""
        return (self.initial == level) or (self.final == level)

    def racah(self, j, jprime, f, fprime, i):
        """The Racah W-coefficient for coupling strength computations"""
        return float(
            (-1) ** (j + 1 + fprime + i)
            * sqrt((2 * fprime + 1) * (2 * j + 1))
            * wigner_6j(j, jprime, 1, fprime, f, i)
        )

    def saturationIntensity(self):
        """Saturation intensity of the transition"""
        return pi / 3 * h * c * self.linewidth * (self.resonance() / c) ** 3

    def resonance(self, B=0, mfi=None, mff=None):
        """the resonance frequency in Hertz

        Args:
            B   : float
                magnetic field (Gauss)
            mfi : int
                magnetic quantum number of inital state
            mff : int
                magnetic quantum number of final state

        Returns:
            the resonance frequency of this transition
        """
        if mfi:
            try:
                self.initial.mf = mfi
            except ValueError:
                return 0 * Hz
                # raise ValueError('Intial mf in resonance computation out of range. mfi=%d'%mfi)
        if mff:
            try:
                self.final.mf = mff
            except:
                return 0 * Hz
                # raise ValueError('Final mf in resonance computation out of range. mff=%d'%mff)
        return self.final.resonance(B) - self.initial.resonance(B)

    def __str__(self):
        return self.initial.__str__() + " --> " + self.final.__str__()

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return (
                self.initial == other.initial
                and self.final == other.final
                and self.coupling == other.coupling
                and self.linewidth == other.linewidth
                and self.resonance == other.resonance
            )
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self.initial) ^ hash(self.final)


@attr.s
class atom:
    """a class for atoms

    Properties:
        m : float
            the mass of the atom in kg
        I : float
            the nuclear spin of the atom (defaults to 7/2)
        levels : list
            a list of lines associated with this atom
        transitions : list
            a list of transitions associated with this atom
    """

    m = attr.ib()  # the mass of the atom
    I = attr.ib(default=7 / 2)  # the nuclear spin of the atom
    levels = attr.ib(default=attr.Factory(list))
    transitions = attr.ib(default=attr.Factory(list))
    Ahfs = attr.ib(default=h * 2.2981579425e9 * Hz)  # needed for Breit Rabi
    gJ = attr.ib(default=2.00254032)  # needed for Breit Rabi
    gI = attr.ib(default=-0.00039885395)  # needed for Breit Rabi

    def addLevel(self, level):
        """add a new line instance to the levels list

        Args:
            level: instance cs.atom.classes.line
                the level to add to the levels list
        """
        assert not any([level == l for l in self.levels]), "level already exists"
        self.levels.append(level)

    def addTransition(self, levela, levelb, coupling, linewidth):
        """add a new transition to the transitions list.

        Args:
            levela: string
                the label one of the level connected by the
                transition
            levelb: string
                the label of the other level connected by the
                transition
            coupling: astropy.Quantity
                the <J|| er ||J'> matrix element.
            linewidth : astropy.Quantity
                the linewidth of the transition (this could be
                calculated from the coupling, but its not)

        Note: levela and levelb have to be added to the level list
            first
        """
        levela = self.levels[
            self.levels.index(levela)
        ]  # find the level objects by labels
        levelb = self.levels[self.levels.index(levelb)]
        if levela.res > levelb.res:
            levela, levelb = levelb, levela  # switch levels is levela has higher energy

        self.transitions.append(transition(levela, levelb, coupling, linewidth, self.I))

    def findLevel(self, level):
        """find a level of atom by string and return the instance

        Args:
            level : string
                the level to be found. Returns the level instance in the
                level list.
        """
        return self.levels[self.levels.index(level.lower())]

    def findTransition(self, levela, levelb):
        """find a transition of atom by string and return the instance"""
        ts = [t for t in self.transitions if t.hasLevel(levela) and t.hasLevel(levelb)]
        try:
            return ts[0]
        except:
            return None

    def makeLtrans(self):
        """partition the transitions list into fine structure
        manifolds"""

        def equivrel(x, y):
            return (
                x.initial.getManifoldLabel() == y.initial.getManifoldLabel()
                and x.final.getManifoldLabel() == y.final.getManifoldLabel()
            )

        self.Ltrans = equivalence_partition_nohash(self.transitions, equivrel)

    def potentialFunc(self, l: m):
        """compute the potential the atom sees

        Note: computed the potential ignoring finestructure. This is
            sufficient if you are detuned from any resonance by much
            more than the finestructure splitting (less than 1nm still
            gives excellent results in Cs)

        Args:
            l : float
                the wavelength in meters of the light

        Returns:
            V : float
                the potential the atom sees, normalized to an
                intensity of 1 W/m^2
        """
        w, E, G, factor = c / l, [], [], []
        for x in self.Ltrans:
            E.append(np.mean([y.resonance().si.value for y in x]))
            G.append(np.mean([y.linewidth.si.value for y in x]))
            factor.append((2 * x[0].final.J + 1) / (2 * x[0].initial.J + 1))
        E, G, factor = np.asarray(E) * Hz, np.asarray(G) * Hz, np.asarray(factor)
        return -np.sum(
            3
            * np.pi
            * c ** 2
            / 2
            / (E * 2 * np.pi) ** 3
            * G
            * (1 / (E - w) + 1 / (w + E))
            * factor
        ) / np.sum(factor)

    def scatteringFunc(self, l: m):
        """compute the scattering rate the atoms sees

        Note: computed the scattering rate ignoring finestructure. This is
            sufficient if you are detuned from any resonance by much
            more than the finestructure splitting (less than 1nm still
            gives excellent results in Cs)

        Args:
            l : float
                the wavelength in meters of the light

        Returns:
            V : float
                the scattering rate the atom sees, normalized to an
                intensity of 1 W/m^2
        """
        w, E, G, factor = c / l, [], [], []
        for x in self.Ltrans:
            E.append(np.mean([y.resonance().si.value for y in x]))
            G.append(np.mean([y.linewidth.si.value for y in x]))
            factor.append((2 * x[0].final.J + 1) / (2 * x[0].initial.J + 1))
        E, G, factor = np.asarray(E) * Hz, np.asarray(G) * Hz, np.asarray(factor)
        return -np.sum(
            3
            * np.pi
            * c ** 2
            / 2
            / pc.hbar
            / (E * 2 * np.pi) ** 3
            * G ** 2
            * (1 / (E - w) + 1 / (w + E)) ** 2
            * (w / E) ** 3
            * factor
        ) / np.sum(factor)

    def polarizabilityFunc(self, *args, **kwargs):
        """compute polarizability of atom according to self.potentialFunc."""
        return self.potentialFunc(*args, **kwargs) * 2 * c * eps0

    def recoil_energy(
        self, omega: Optional[u.Unit] = None, *, wavelength: Optional[u.Unit] = None
    ):
        """compute the recoil energy of the atom"""
        if omega is None:
            if wavelength is not None:
                omega = 2 * np.pi * c / wavelength.to(u.m)
            else:
                raise TypeError("either omega or wavelength must be given!")

        return (hbar ** 2 * omega.to(u.Hz) ** 2 / (2 * self.m * c ** 2)).to(u.J)

    @quantity_input
    def lightshift(self, levellabel, mfinit, frequency: Hz, polarisation):
        """
        compute the lightshift an atom in state levellabel experiences.

        Args:
            levellabel : string or cs.atom.classes.line instance
                the label of atoms state. If you are passing a line
                instance, make sure that the line exists in
                atom.levels
            mfinit     : int
                the magnetic quantum number of the atoms state
            frequency  : astropy.Quantity
                the frequency of the laser in Hertz
            polarisation : int
                the polarisation of the laser encoded as:
                -1, 0, 1 : s+, pi, s-

        Returns:
            V : float
                the lightshift normalized to an electric field of
                1 V/m. The actual lightshift is found by multiplying
                this value with the squared electric field strength
                of the light
        """
        try:
            level = self.findLevel(levellabel)
        except AttributeError:  # maybe level is not a string, but an instance of line
            level = levellabel
        trans = level.findTransitions(self)
        func = lambda t: (
            t.dipole(polarisation, mfinit, mfinit - polarisation) ** 2
            / (frequency - t.resonance())
        )
        return np.sum(Quantity(list(map(func, trans))), axis=0)

    def polarizability(self, *args, **kwargs):
        """
        compute the polarizability of an atom according to self.lightshift.
        To calculate the potential U use:
        $$ U = 1/(2*c*eps0) * polarizability * I $$
        """
        return self.lightshift(*args, **kwargs) / h

    def ramanTransition(self, level1, level2, mf1, mf2, f1, pol1, f2, pol2):
        """
        find the rabi frequency of a raman transition between two states

        Args:
            level1 : string, cs.atom.classes.line instance
                the label of the first state
            level2 : string, cs.atom.classes.line instance
                the label of the second state
            mf1 : int
                the magnetic quantum number of the initial state
            mf2 : int
                the magnetic quantum number of the final state
            f1 : float
                the frequency of the first raman laser in Hertz
            pol1 : int
                the polarisation of the first raman laser
            f2 : float
                the frequency of the second raman laser
            pol2 :
                the polarisation of the second raman laser

        Returns:
            V : float
                the lightshift normalized to an electric field of
                1 V/m. The actual rabi frequency is found by
                multiplying the returned value by the product of the
                electric fields of the first and second raman lasers.
        """
        try:
            l1 = self.findLevel(level1)
        except AttributeError:  # maybe l1 is not a string, but an instance of line
            l1 = level1
        try:
            l2 = self.findLevel(level2)
        except AttributeError:  # maybe l2 is not a string, but an instance of line
            l2 = level2
        t1 = l1.findTransitions(self)
        d = 0.0
        for t in t1:
            t2 = self.findTransition(l2, t.final)  # find the return transition

            d1 = t.dipole(pol1, mf1, mf1 - pol1)  # inital excitation by beam1
            d2 = t2.dipole(-pol2, mf2, mf1 - pol1 + pol2)
            d += d1 * d2 / 2 / (f1 - t.resonance())

            d1 = t.dipole(pol2, mf1, mf1 - pol2)  # inital excitation by beam2
            d2 = t2.dipole(-pol1, mf2, mf1 - pol2 + pol1)
            d += d1 * d2 / 2 / (f2 - t.resonance())

        return d

    def breitRabi(self, F, mF, B):
        """compute the Breit rabi energy of a given F, mF state of the ground state of the atoms (J=1/2)"""

        J = 0.5
        mJ = np.arange(2 * J + 1) - J
        mI = np.arange(2 * self.I + 1) - self.I
        IJBasisStates = [(J, mj, self.I, mi) for mj in mJ for mi in mI]

        def decompose(F, mF):
            decomposition = []
            for i, v in enumerate(IJBasisStates):
                decomposition.append(cg.CG(*v, F, mF))
            return decomposition

        def breitrabi(mJ, I, mI, B, F):
            Ehfs = self.Ahfs * (I + 0.5)
            x = (self.gJ - self.gI) * muB * B / Ehfs
            m = mI + mJ
            term1 = -Ehfs / 2 / (2 * I + 1)
            term2 = self.gI * muB * m * B
            term3 = Ehfs / 2 * np.sqrt(1 + 4 * m * x / (2 * I + 1) + x ** 2)
            if m == I + 0.5:  # and F==4:
                return (
                    Ehfs * I / (2 * I + 1) + 0.5 * (self.gJ + 2 * I * self.gI) * muB * B
                )
            elif m == -I - 0.5:  # and F==4:
                return (
                    Ehfs * I / (2 * I + 1) - 0.5 * (self.gJ + 2 * I * self.gI) * muB * B
                )
            if F == 3:
                term3 *= -1
            return term1 + term2 + term3

        decomposition = decompose(F=F, mF=mF)
        energies = [
            breitrabi(v[1], v[2], v[3], B, F) * float(d.doit()) ** 2
            for v, d in zip(IJBasisStates, decomposition)
        ]
        return sum(energies)
