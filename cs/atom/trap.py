from collections.abc import MutableSequence

import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import find_peaks

import cs.constants as pc
import cs.units as u
from cs.atom.caesium import cs
from cs.vector import rotation_from_vectors


def intensity(P, w):
    """Gaussian intensity at focus"""
    return 2 * P / np.pi / w ** 2


def radialTrapFreq(P, w, l):
    """Radial trap frequency"""
    u0 = cs.potentialFunc(l)
    return np.sqrt(4 * np.abs(u0) * intensity(P, w) / pc.cs.m / w ** 2)


def axialTrapFreq(P, w, l):
    """Axial trap frequency"""
    zR = np.pi * w ** 2 / l
    u0 = cs.potentialFunc(l)
    return np.sqrt(2 * np.abs(u0) * intensity(P, w) / pc.cs.m / zR ** 2)


def ldb(T):
    return pc.h / np.sqrt(2 * np.pi * pc.cs.m * pc.k_B * T)


def thermal_velocity(T):
    """rms velocity in 3D"""
    return np.sqrt(3 * pc.k_B * T / pc.cs.m)


class trap:
    def __init__(self, x, y, z, **kwargs):
        self.x = x
        self.y = y
        self.z = z
        self.params = dict(**kwargs)
        self.trap_compute(**self.params)
        self.analytical_trap_freqs(**self.params)
        self.trap_freq()

    def find_minimum(self, gradient, ix, iy, iz):
        gradient = np.array(np.sign(gradient), dtype=int)
        step = [0, 0, 0]
        s = gradient.shape
        while True:  # i in range(10):
            if ix < 0 or iy < 0 or iz < 0:
                ix, iy, iz = max(ix, 0), max(iy, 0), max(iz, 0)
                break
            if ix >= s[1] or iy >= s[2] or iz >= s[3]:
                ix, iy, iz = min(ix, s[1] - 1), min(iy, s[2] - 1), min(iz, s[3] - 1)
                break
            if np.any(step != -gradient[:, ix, iy, iz]):
                step = gradient[:, ix, iy, iz]
            else:
                break
            ix, iy, iz = ix - step[0], iy - step[1], iz - step[2]
        return ix, iy, iz

    def find_maximum(self, gradient, ix, iy, iz, trap):
        """find the possible local maximum below or above the center of the trap"""
        gradient = np.array(gradient)
        p, _ = find_peaks(np.abs(gradient[2, ix, iy, :]))
        indices = []
        for peak in p:
            if peak < iz:
                i = np.argmin(np.abs(gradient[2, ix, iy, :peak]))
            else:
                i = np.argmin(np.abs(gradient[2, ix, iy, peak:])) + peak
            indices.append(i)
        try:
            return indices[
                np.argmin(np.abs(trap[ix, iy, np.asarray(indices)] - trap[ix, iy, iz]))
            ]  # return closest peak to iz
        except (UnboundLocalError, ValueError, IndexError):
            return None

    def trap_freq(self):
        x = self.x
        y = self.y
        z = self.z
        trap = self.trap

        i, j, k = (s // 2 for s in x.shape)
        dx = (x[1, 0, 0] - x[0, 0, 0]).to(u.m).value
        dy = (y[0, 1, 0] - y[0, 0, 0]).to(u.m).value
        dz = (z[0, 0, 1] - z[0, 0, 0]).to(u.m).value

        gradient = np.gradient(trap.to(u.J).value, dx, dy, dz)
        i, j, k = self.find_minimum(gradient, i, j, k)
        sndgradient = np.asarray([np.gradient(g, dx, dy, dz) for g in gradient])
        w, v = np.linalg.eig(sndgradient[:, :, i, j, k] / 2)

        self.trap_f = np.sqrt(2 * w * (u.J / u.m ** 2) / cs.m).to(u.Hz) / (2 * np.pi)
        self.geo_trap_f = (self.trap_f[0] * self.trap_f[1] * self.trap_f[2]) ** (1 / 3)
        self.trap_w = self.trap_f * 2 * np.pi
        self.geo_trap_w = self.geo_trap_f * 2 * np.pi

    def trap_depth(self):
        x = self.x
        y = self.y
        z = self.z
        trap = self.trap

        i, j, k = (s // 2 for s in x.shape)
        dx = (x[1, 0, 0] - x[0, 0, 0]).to(u.m).value
        dy = (y[0, 1, 0] - y[0, 0, 0]).to(u.m).value
        dz = (z[0, 0, 1] - z[0, 0, 0]).to(u.m).value

        gradient = np.gradient(trap.to(u.J).value, dx, dy, dz)
        i, j, k = self.find_minimum(gradient, i, j, k)
        trapmin = trap[i, j, k]
        k = self.find_maximum(gradient, i, j, k, trap)
        if k is not None:
            trapmax = trap[i, j, k]
        else:
            trapmax = trap.max()

        return trapmax - trapmin

    def __add__(self, other):
        assert self.x.shape == other.x.shape
        assert self.y.shape == other.y.shape
        assert self.z.shape == other.z.shape

        newtrap = trap(self.x, self.y, self.z)
        newtrap.trap = self.trap + other.trap
        newtrap.params.update(self.params)
        newtrap.params.update(other.params)

        newtrap.ana_trap_w = np.sqrt(
            np.sign(self.ana_trap_w) * self.ana_trap_w ** 2
            + np.sign(other.ana_trap_w) * other.ana_trap_w ** 2
        )
        newtrap.ana_geo_w = np.prod(newtrap.ana_trap_w.value) ** (1 / 3) * u.Hz
        newtrap.ana_trap_f = newtrap.ana_trap_w / 2 / np.pi
        newtrap.ana_geo_f = newtrap.ana_geo_w / 2 / np.pi

        newtrap.trap_freq()

        return newtrap

    def trap_compute(self, **kwargs):
        self.trap = np.zeros(self.x.shape) * u.J

    def analytical_trap_freqs(self, **kwargs):
        self.ana_trap_w = np.asarray((0, 0, 0)) * u.Hz
        self.ana_geo_w = np.prod(self.ana_trap_w.value) ** (1 / 3) * u.Hz
        self.ana_trap_f = self.ana_trap_w / 2 / np.pi
        self.ana_geo_f = self.ana_geo_w / 2 / np.pi

    def plot(self):
        fig = plt.figure(figsize=(21, 7))
        xc, yc, zc = (s // 2 for s in self.x.shape)  # centers
        fig.add_subplot(131, aspect="equal")
        plt.contourf(
            self.x[:, :, zc],
            self.y[:, :, zc],
            self.trap[:, :, zc],
            levels=10,
            cmap="jet",
        )
        plt.xlabel("x")
        plt.ylabel("y")
        fig.add_subplot(132, aspect="equal")
        plt.contourf(
            self.x[:, yc, :],
            self.z[:, yc, :],
            self.trap[:, yc, :],
            levels=10,
            cmap="jet",
        )
        plt.xlabel("x")
        plt.ylabel("z")
        fig.add_subplot(133, aspect="equal")
        plt.contourf(
            self.y[xc, :, :],
            self.z[xc, :, :],
            self.trap[xc, :, :],
            levels=10,
            cmap="jet",
        )
        plt.xlabel("y")
        plt.ylabel("z")

    def density(self, N, T):
        return N * self.geo_trap_w ** 3 * (pc.cs.m * ldb(T) / pc.h) ** 3

    def psd(self, N, T):
        return self.density(N, T) * ldb(T) ** 3

    def sigma_k(self, T, a):
        try:
            a.unit
        except (SyntaxError, AttributeError):
            a = a * pc.a0
        return (
            8
            * np.pi
            * a ** 2
            / (1 + pc.cs.m ** 2 / pc.hbar ** 2 * a ** 2 * thermal_velocity(T) ** 2)
        )

    def thermalization_rate(self, N, T, a):
        return self.density(N, T) * self.sigma_k(T, a) * thermal_velocity(T)

    def eta(self, N, T, N0, phi0):
        return np.log(self.psd(N, T) / phi0) / np.log(N0 / N)

    def Tc(self, N):
        return 7.17993e-12 * self.geo_trap_w.si.value ** (1 / 3) * N * u.K

    def N_BEC(self, N, T, eta=2, phiBEC=3):
        return N / np.exp(np.log(phiBEC / self.psd(N, T)) / eta)

    def make_report(self, N, T, a):
        return dict(
            n=self.density(N, T).to(1 / u.cm ** 3),
            phi=self.psd(N, T).si,
            sigma_k=self.sigma_k(T, a).to(u.um ** 2),
            collision_rate=self.thermalization_rate(N, T, a).to(1 / u.s),
            Tc=self.Tc(N).to(u.uK),
            ldb=ldb(T).to(u.um),
            vT=thermal_velocity(T),
            N=N,
            T=T.to(u.uK),
            a=a,
            trapdepth=(self.trap_depth() / pc.k_B).to(u.uK),
            hydroparam=(self.thermalization_rate(N, T, a) / self.geo_trap_w).si,
            N_BEC=self.N_BEC(N, T, eta=2, phiBEC=3),
        )

    def print_report(self, N, T, a0):
        reportFormat = (
            "{N:>7.1e}, {T:>3.1f}, {a:>4.0f}, {n:>8.1e}, {phi:>7.1e},"
            " {trapdepth:>7.1f}, {Tc:>5.1f}, {f[0]:>5.0f}, {f[1]:>5.0f}, {f[2]:>5.0f},"
            " {N_BEC:>7.1e}"
        )
        reportHeader = (
            "{:^7}, {:^7}, {:^3}, {:^16}, {:^7}, {:^10}, {:^8}, {:^8}, {:^8}, {:^8},"
            " {:^7}".format(
                "N",
                "T",
                "a",
                "n",
                "phi",
                "trapdepth",
                "Tc",
                "nu1",
                "nu2",
                "nu3",
                "N_BEC",
            )
        )
        print(reportHeader)
        print(reportFormat.format(**self.make_report(N, T, a0), f=self.trap_f))


class gausstrap(trap):
    def __init__(self, x, y, z, wx0, wy0, p, l, theta=np.pi / 2, phi=0):
        super().__init__(x, y, z, wx0=wx0, wy0=wy0, l=l, power=p, theta=theta, phi=phi)

    def trap_compute(self, wx0, wy0, l, power, theta=np.pi / 2, phi=0):
        x, y, z = self.x, self.y, self.z

        r = np.array((x.si.value, y.si.value, z.si.value)) * u.m
        n = np.asarray(
            (np.sin(theta) * np.cos(phi), np.sin(theta) * np.sin(phi), np.cos(theta))
        )
        n = rotation_from_vectors(n, (0, 0, 1))
        r2 = (r.T @ n).T

        x, y, z = r2[0, ...], r2[1, ...], r2[2, ...]

        zrx = np.pi * wx0 ** 2 / l
        zry = np.pi * wy0 ** 2 / l
        wx = wx0 * np.sqrt(1 + (z / zrx) ** 2)
        wy = wy0 * np.sqrt(1 + (z / zry) ** 2)

        self.trap = (
            2
            * power
            / wx
            / wy
            / np.pi
            * np.exp(-2 * (x ** 2 / wx ** 2 + y ** 2 / wy ** 2))
            * cs.potentialFunc(l)
        )

    def analytical_trap_freqs(self, wx0, wy0, l, power, theta, phi):
        u0 = cs.potentialFunc(l)
        I = 2 * power / wx0 / wy0 / np.pi
        omx = np.sqrt(4 * np.abs(u0) * I / pc.cs.m / wx0 ** 2)
        omy = np.sqrt(4 * np.abs(u0) * I / pc.cs.m / wy0 ** 2)
        zrx = np.pi * wx0 ** 2 / l
        zry = np.pi * wy0 ** 2 / l
        omz = np.sqrt(2 * np.abs(u0) * I / pc.cs.m / min(zrx, zry) ** 2)

        self.ana_trap_w = (
            np.asarray((omx.to(u.Hz).value, omy.to(u.Hz).value, omz.to(u.Hz).value))
            * u.Hz
        )
        n = np.asarray(
            (np.sin(theta) * np.cos(phi), np.sin(theta) * np.sin(phi), np.cos(theta))
        )
        n = rotation_from_vectors(n, (0, 0, 1))
        self.ana_trap_w = np.abs((self.ana_trap_w.T @ n).T)

        self.ana_geo_w = np.prod(self.ana_trap_w.value) ** (1 / 3) * u.Hz
        self.ana_trap_f = self.ana_trap_w / 2 / np.pi
        self.ana_geo_f = self.ana_geo_w / 2 / np.pi


class levitation(trap):
    def __init__(self, x, y, z, gradient, offset, mf):
        super().__init__(x, y, z, gradient=gradient, offset=offset, mf=mf)

    def trap_compute(self, mf, gradient, offset):
        self.trap = (
            mf
            * pc.muB
            * -1
            / 4
            * np.sqrt(
                (self.x * gradient / 2) ** 2
                + (self.y * gradient / 2) ** 2
                + ((self.z * gradient + offset) ** 2)
            )
        )

    def analytical_trap_freqs(self, mf, gradient, offset):
        omz = 0 * u.Hz
        omr = -np.sqrt(mf * pc.muB / 4 / (4 * pc.cs.m * offset) * gradient ** 2)

        self.ana_trap_w = (
            np.asarray((omr.to(u.Hz).value, omr.to(u.Hz).value, omz.to(u.Hz).value))
            * u.Hz
        )
        self.ana_geo_w = np.prod(self.ana_trap_w.value) ** (1 / 3) * u.Hz
        self.ana_trap_f = self.ana_trap_w / 2 / np.pi
        self.ana_geo_f = self.ana_geo_w / 2 / np.pi


class gravity(trap):
    def __init__(self, x, y, z):
        super().__init__(x, y, z)

    def trap_compute(self, **kwargs):
        self.trap = pc.cs.m * 9.81 * u.m / u.s ** 2 * self.z


class evaporation_ramp(
    MutableSequence
):  # gives append method assuming enough functions are defined
    def __init__(self, trap=None, duration=None, powers=None):
        self.traps = [trap]
        self.durations = np.array([duration.si.value]) * u.s
        self.powers = np.array(([p.to(u.W).value for p in powers],)) * u.W

    def plot(self):
        fig = plt.figure()
        fig.add_subplot(121)
        plt.plot(
            np.cumsum(self.durations),
            [(t.trap_depth() / pc.k_B).to(u.uK).value for t in self.traps],
        )
        plt.xlabel("Time")
        plt.ylabel("Trap depth")

        fig.add_subplot(122)
        plt.plot(np.cumsum(self.durations), self.powers)
        plt.xlabel("Time")
        plt.ylabel("Laser powers")

    def extend_ramp(self, trap, duration, powers):
        self.durations = (
            np.concatenate((self.durations.si.value, (duration.si.value,))) * u.s
        )
        self.traps.append(trap)
        self.powers = (
            np.concatenate(
                (self.powers.to(u.W).value, ([p.to(u.W).value for p in powers],)),
                axis=0,
            )
            * u.W
        )

    def make_report(self, Ns, Ts, a0s):
        reports = [
            t.make_report(N, T, a) for t, N, T, a in zip(self.traps, Ns, Ts, a0s)
        ]
        efficiencies = [
            t.eta(N, T, Ns[0], self.traps[0].psd(Ns[0], Ts[0]))
            for t, N, T in zip(self.traps[1:], Ns[1:], Ts[1:])
        ]
        reportFormat = (
            "{N:>7.1e}, {T:>3.1f}, {a:>4.0f}, {n:>8.1e}, {phi:>7.1e},"
            " {trapdepth:>7.1f}, {Tc:>5.1f}, {f[0]:>5.0f}, {f[1]:>5.0f}, {f[2]:>5.0f},"
            " {N_BEC:>7.1e}"
        )
        reportHeader = (
            "{:^7}, {:^7}, {:^3}, {:^16}, {:^7}, {:^10}, {:^8}, {:^8}, {:^8}, {:^8},"
            " {:^7}".format(
                "N",
                "T",
                "a",
                "n",
                "phi",
                "trapdepth",
                "Tc",
                "nu1",
                "nu2",
                "nu3",
                "N_BEC",
            )
        )
        print(reportHeader)
        for report, t in zip(reports, self.traps):
            print(reportFormat.format(**report, f=t.trap_f))

        print("Efficiencies:")
        print(", ".join([f"{eff:.2f}" for eff in efficiencies]))

    def __len__(self):
        return len(self.traps)

    def __getitem__(self, item):
        return self.traps[item], self.durations[item], self.powers[item]

    def __setitem__(self, key, value):
        self.traps[key] = value[0]
        self.durations[key] = value[1]
        self.powers[key] = value[2]

    def __delitem__(self, key):
        self.traps = np.delete(self.traps, key)
        self.durations = np.delete(self.durations, key)
        self.powers = np.delete(self.powers, key, axis=0)

    def insert(self, key, value):
        self.traps = np.insert(self.traps, key, value[0])
        self.durations = np.insert(self.durations, key, value[1])
        self.powers = np.insert(self.powers, key, value[2], axis=0)


if __name__ == "__main__":
    phi = 30 * np.pi / 180
    phi = np.pi / 2
    theta = np.pi / 2
    x, y, z = np.mgrid[-150:150:100j, -150:150:100j, -150:150:100j] * u.um

    w1 = 200 * u.um
    w2 = 300 * u.um
    p1 = 35 * u.W
    p2 = 8 * u.W
    l = 1064 * u.nm
    g1 = gausstrap(x, y, z, w1, w1, p1, l, theta)
    g2 = gausstrap(x, y, z, w2, w2, p2, l, theta, phi)
    lev = levitation(x, y, z, mf=3, gradient=31.1 * u.G / u.cm, offset=20 * u.G)
    grav = gravity(x, y, z)

    fulltrap = g1 + g2 + lev + grav

    print(g1.ana_trap_f)
    g1.trap_freq()
    print(g1.trap_f)

    print(g2.ana_trap_f)
    g2.trap_freq()
    print(g2.trap_f)

    print(fulltrap.ana_trap_f)
    fulltrap.trap_freq()
    print(fulltrap.trap_f)

    fulltrap.plot()

    ramp = evaporation_ramp(fulltrap, 1 * u.s, (p1, p2))
    for p2, time in zip(
        np.asarray((8, 2, 0.5, 0.1, 0)) * u.W, np.asarray((1, 1, 1, 1)) * u.s
    ):
        t = g1 + gausstrap(x, y, z, w2, w2, p2, l, theta, phi) + lev + grav
        print(t.trap_f)
        # ramp.extend_ramp(t, duration=time, powers=(p1,p2))
        ramp.append((t, time, (p1, p2)))

    ramp.plot()

    fulltrap.trap_depth()
    ramp.make_report(
        Ns=np.asarray((5, 3, 1, 0.5, 0.3, 0.2)) * 1e6,
        Ts=np.asarray((6, 3, 1, 0.5, 0.1, 0.05)) * u.uK,
        a0s=np.asarray((500, 220, 220, 220, 220, 220)),
    )

    plt.show()
