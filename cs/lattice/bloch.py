from typing import Tuple

import numpy as np
from optical_traps import *
import cs.units as u
import cs.constants as pc
from cs.atom.caesium import cs


def fourierAxis(nx, dx):
    return np.fft.fftshift(np.fft.fftfreq(nx, dx)) * 2 * np.pi


def ft(arr, ax=1):
    return np.fft.ifftshift(np.fft.fft(np.fft.fftshift(arr, axes=ax), axis=ax), axes=ax)


def fastBlochLattice(vdepth: float, nx: int = 128):
    """numerically compute bloch lattice parameters

    formulas based on Markus Grainers thesis

    Parameters
    ----------
    vdepth: float
        potential depth

    nx: int, default=128
        sampling of k, i.e. points between each lattice site

    Returns
    -------

    Attributes
    ----------
    nsites: int
        sampling of q, total number of lattice sites
    """
    nsites = 4  # must be even
    k = np.arange(nx) - nx // 2  # fourierAxis(nx, period / nx)
    q = (np.arange(nx) - nx // 2) / nx
    v0 = -0.25 * vdepth * (np.eye(nx, k=-1) + np.eye(nx, k=1))

    # create q matrix
    qtmp = np.zeros((nx, nx, nx))
    idx = np.arange(nx)
    idx2 = np.tile(idx, (nx, 1)).T
    qtmp[:, idx, idx] = q[idx2[idx]]

    # create k matrix
    ktmp = np.tile(np.diag(k), (nx, 1, 1))
    vtmp = np.tile(v0, (nx, 1, 1))

    e, w = np.linalg.eig(
        4 * (ktmp - qtmp) ** 2 + vtmp
    )  # diagonalize along last two dims

    ii = np.argsort(e)

    w3 = np.zeros(w.shape)
    for ix, (ixx, ww) in enumerate(
        zip(ii, w)
    ):  # use the argsort array to sort eigenvectors and values
        w3[ix] = ww[:, ixx]
    e = np.take_along_axis(e, ii, axis=-1)

    ux = ft(w3, 1)  # real space representation of eigenfunctions
    ux /= np.linalg.norm(ux, axis=1, keepdims=True)

    # sign adjustment necessary for Wannier function computation
    ix = np.where(np.mean(ux, axis=1) > 0)
    ux[ix[0], :, ix[1]] *= -1

    # wannier function computation
    xtot = np.arange(-(nsites + 1) / 2, (nsites - 1) / 2, 1 / nx)

    global wannier
    wannier = np.sum(
        np.tile(ux, (1, nsites, 1))
        * np.exp(1j * 2 * np.pi * np.outer(q, xtot))[..., np.newaxis],
        axis=0,
    )
    wannier /= np.linalg.norm(wannier, axis=0) * np.sqrt(1 / nx)

    f = e[nx // 2, 1] - e[nx // 2, 0]
    J = 0.25 * (e[:, 0].max() - e[:, 0].min())
    dx = xtot[1] - xtot[0]
    wannieroverlap = np.sum(np.abs(wannier[:, 0]) ** 4) * dx
    return f, J, e, xtot, wannier, wannieroverlap


def makelattice(p, lam, w1, w2, angle):
    try:
        p = p.si.value
        lam = lam.si.value
        w1 = w1.si.value
        w2 = w2.si.value
    except:
        pass
    lat_beam_1 = LaserBeam(lam, p, w1, w2, angle)
    lat_beam_2 = LaserBeam(lam, p, w1, w2, -angle)
    lattice = AccordionLattice(lat_beam_1, lat_beam_2)
    return lattice


def ju_blochlattice(
    U0: float, wavelength: u.Unit, a: float, angle: float = np.pi / 2
) -> Tuple[float, float, float]:
    """compute J and U for a 1D Bloch lattice

    Parameters
    ----------
    U0
    wavelength: Unit[m]
        laser beam wavelength in m
    a: float
        scattering length
    angle: float, default: np.pi/2
        interference angle of the two beams, pi/2 for retro-reflected beams,
        smaller for angled lattices

    Returns
    -------
    f, J, U, trapping frequency, hopping and on-site interaction
    """
    dlatt = wavelength / np.sin(angle) / 2
    er = cs.recoil_energy(wavelength=2 * dlatt)
    v0 = (U0 / er).si.value
    f, J, _, _, _, woverlap = fastBlochLattice(v0, 128)
    f = f * er / pc.h
    J = J * er / pc.h
    woverlap = woverlap / (dlatt)
    if v0 < 1e6:
        U = (pc.hbar ** 2 * 4 * np.pi * a / pc.cs.m * woverlap ** 3) ** (1 / 3)
    else:
        om = np.sqrt(2 * v0 * er * (2 * np.pi / dlatt / 2) ** 2 / pc.cs.m)
        U = (
            pc.hbar ** 2
            * 4
            * np.pi
            * a
            / pc.cs.m
            * np.sqrt(pc.cs.m / 2 / np.pi / pc.hbar) ** 3
            * (om) ** (3 / 2)
        ) ** (1 / 3)

    return f, J, U


def ju_from_v0Andd(Vxy, dxy, Vz, dz, a):
    er = cs.recoil_energy(wavelength=2 * dxy)
    fxy, Jxy, _, _, _, woverlap = fastBlochLattice(Vxy, 128)
    fxy = fxy * er / pc.h
    Jxy = Jxy * er / pc.h
    woverlap = woverlap / dxy
    if Vxy < 1e6:
        Uxy = (pc.hbar ** 2 * 4 * np.pi * a / pc.cs.m * woverlap ** 3) ** (1 / 3)
    else:
        om = np.sqrt(2 * Vxy * er * (2 * np.pi / dxy / 2) ** 2 / pc.cs.m)
        Uxy = (
            pc.hbar ** 2
            * 4
            * np.pi
            * a
            / pc.cs.m
            * np.sqrt(pc.cs.m / 2 / np.pi / pc.hbar) ** 3
            * (om) ** (3 / 2)
        ) ** (1 / 3)

    er = cs.recoil_energy(wavelength=2 * dz)
    fz, Jz, _, _, _, woverlap = fastBlochLattice(Vz, 128)
    fz = fz * er / pc.h
    Jz = Jz * er / pc.h
    woverlap = woverlap / (dz)
    if Vz < 1e6:
        Uz = (pc.hbar ** 2 * 4 * np.pi * a / pc.cs.m * woverlap ** 3) ** (1 / 3)
    else:
        om = np.sqrt(2 * Vz * er * (2 * np.pi / dz / 2) ** 2 / pc.cs.m)
        Uz = (
            pc.hbar ** 2
            * 4
            * np.pi
            * a
            / pc.cs.m
            * np.sqrt(pc.cs.m / 2 / np.pi / pc.hbar) ** 3
            * (om) ** (3 / 2)
        ) ** (1 / 3)

    return fxy, Jxy, fz, Jz, (Uxy ** 2 * Uz / pc.h).to(u.Hz)


def latticeparams(lattice, a=None):
    wavelength = lattice.beam_1.wavelength * u.m
    pol = cs.polarizability(
        levellabel="2s1/2f3", mfinit=3, frequency=pc.c / wavelength, polarisation=0
    ).si.value
    (wx, wy, wz), wbar, U0 = lattice.calc_trap_params(pol, m_atoms=pc.atoms.cs.m.value)
    Er = cs.recoil_energy(wavelength=wavelength / np.sin(lattice.beam_1.angle))

    wx *= u.Hz
    wy *= u.Hz
    wz *= u.Hz
    wbar *= u.Hz
    U0 *= u.J
    if a is not None:
        oma, j_er, u_er = ju_blochlattice(
            np.abs(U0).si, wavelength, a, angle=lattice.beam_1.angle
        )
        return wx, wy, wz, U0, Er, oma, j_er, u_er
    return wx, wy, wz, U0, Er


def lattice2d(lattx, lattz, a):
    wx, wy, wz, U0, Er, oma, jxy, uxy = latticeparams(makelattice(**lattx), a)
    wxz, wyz, wzz, U0z, Erz, omaz, jz, uz = latticeparams(makelattice(**lattz), a)
    fr = (Er / pc.h).to(u.Hz)
    frz = (Erz / pc.h).to(u.Hz)

    from types import SimpleNamespace

    l2d = SimpleNamespace()
    l2d.fbare_xy = (wx / 2 / np.pi, wy / 2 / np.pi, wz / 2 / np.pi)
    l2d.fbare_z = (wxz / 2 / np.pi, wyz / 2 / np.pi, wzz / 2 / np.pi)
    # assume square lattice, and hope that waists are given in correct order:
    l2d.fx = np.sqrt(wy ** 2 + wyz ** 2) / 2 / np.pi
    l2d.fy = np.sqrt(wy ** 2 + wzz ** 2) / 2 / np.pi
    l2d.fz = np.sqrt(2) * wz / 2 / np.pi

    l2d.V0xy = U0
    l2d.V0z = U0z

    l2d.fxy = wx / 2 / np.pi
    l2d.fzz = wxz / 2 / np.pi

    l2d.Jxy = jxy.to(u.Hz)
    l2d.Jz = jz.to(u.Hz)

    l2d.U = (uxy ** 2 * uz / pc.h).to(u.Hz)

    l2d.Er = Er
    l2d.Erz = Erz

    return l2d


lattfmt = """{}: 
  Power: {p:.2g}, Wavelength: {lam:.0f}, 
  Waist: {w1:.0f}x{w2:.0f}, angle: {angle:.3f} rad"""
l1d_fmt = """1D lattice {}, bare frequencies: 
  fx,fy,fz: {:.2f}, {:.2f}, {:.2f}"""
l2d_fmt = """2D lattice:
  fx,fy,fz: {:.2f}, {:.2f}, {:.2f}
  V0xy,V0z: {:.2f}, {:.2f} Er, {:.2f}, {:.2f}, 
  fxy, fz: {:.2f}, {:.2f}
  Jxy, Jz: {:.2f}, {:.2f} 
  U, U/Jxy: {:.2f}, {:.2f} """
mott_fmt = """Mott size: 
  RX = {rdax:.1f} = {Nsitesx:.1f} sites, 
  RY = {rday:.1f} = {Nsitesy:.1f} sites
  Atom Number: {N:.0f}"""
TF_fmt = """Thomas Fermi Size: 
  RTFX = {RTFx:.1f}, 
  RTFX = {RTFy:.1f}"""
dipole_fmt = """Crossed Dipole trap:
  V = {:.1f}, {:.1f}
  f1 = {:.1f}, f2 = {:.1f}, f3 = {:.1f}
  """
ju_fromV0_fmt = """Vxy: {:.1f} Er, dxy: {:.1f}, 
Vz: {:.1f} Er, dz: {:.1f}, 
a: {:.1f} a0
  fxy, fz: {:.2f}, {:.2f}
  Jxy, Jz: {:.2f}, {:.2f} 
  U, U/Jxy: {:.2f}, {:.2f}
"""


def print_dipole(d1, d2):
    v1, v2, om1, om2, om3 = crosseddipoletrap(d1, d2)
    print(
        dipole_fmt.format(
            (v1 / pc.k_B).to(u.uK),
            (v2 / pc.k_B).to(u.uK),
            om1.to(u.Hz) / 2 / np.pi,
            om2.to(u.Hz) / 2 / np.pi,
            om3.to(u.Hz) / 2 / np.pi,
        )
    )


def print_latt2d(lattxy, lattz, a):
    l2d = lattice2d(lattxy, lattz, a)
    print(lattfmt.format("Vxy", **lattxy))
    print(lattfmt.format("Vz", **lattz))
    print(f"\na={(a / pc.a0).si:.2f} a0\n")
    print(l1d_fmt.format("XY", *l2d.fbare_xy))
    print(l1d_fmt.format("Z", *l2d.fbare_z))
    print(
        l2d_fmt.format(
            l2d.fx.to(u.Hz),
            l2d.fy.to(u.Hz),
            l2d.fz.to(u.Hz),
            (l2d.V0xy / l2d.Er).si,
            (l2d.V0z / l2d.Erz).si,
            (l2d.V0xy / pc.k_B).to(u.uK),
            (l2d.V0z / pc.k_B).to(u.uK),
            l2d.fxy.to(u.Hz),
            l2d.fzz.to(u.Hz),
            l2d.Jxy.to(u.Hz),
            l2d.Jz.to(u.Hz),
            l2d.U.to(u.Hz),
            (l2d.U / l2d.Jxy).si,
        )
    )
    return l2d


def print_ju_fromU0(Vxy, dxy, Vz, dz, a):
    fxy, Jxy, fz, Jz, U = ju_from_v0Andd(Vxy, dxy, Vz, dz, a)
    print(
        ju_fromV0_fmt.format(
            Vxy,
            dxy.to(u.nm),
            Vz,
            dz.to(u.nm),
            (a / pc.a0).si,
            fxy.to(u.Hz),
            fz.to(u.Hz),
            Jxy.to(u.Hz),
            Jz.to(u.Hz),
            U.to(u.Hz),
            (U / Jxy).si,
        )
    )


if __name__ == "__main__":
    xdip = {"p": 50 * u.mW, "lam": 1064 * u.nm, "w1": 100 * u.um, "w2": 100 * u.um}
    stVL = {
        "p": 4 * u.W,
        "lam": 1064 * u.nm,
        "w1": 100 * u.um,
        "w2": 220 * u.um,
        "angle": 30 * np.pi / 180,
    }
    hL1 = {
        "p": 0.015 * u.W,
        "lam": 767 * u.nm,
        "w1": 45 * u.um,
        "w2": 303 * u.um,
        "angle": np.pi / 2,
    }

    import time

    start = time.time()
    global nsites
    nsites = 128
    print_latt2d(hL1, stVL, a=200 * pc.a0)
    end = time.time()
    print(f"{end-start:.2f}s")

    start = time.time()
    nsites = 4
    print_latt2d(hL1, stVL, a=200 * pc.a0)
    end = time.time()
    print(f"{end-start:.2f}s")
