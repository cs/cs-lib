import attr
import numba
import numpy as np
import scipy.constants as pconst
from numba import complex128, float64, int_, jit, njit

from cs.atom.caesium import cs
from cs.units import u, unit_value


@njit(
    numba.types.Tuple((float64[:], complex128[:, :]))(
        float64, float64, float64, float64, int_
    )
)
def bloch_states(k, v_long, v_short, phi, lattice_dim):
    """
    Calculates spectrum and eigenstates of a period two superlattice.
    :param k: quasi momentum (-1, 1]
    :param v_long: lattice depth of the long period lattice in Er_long
    :param v_short: lattice depth of the short period lattice in Er_short
    :param phi: relative superlattice phase (0: symmetric, pi/2: staggered)
    :param lattice_dim: 2 * lattice_dim + 1 is the system size
    :return: eigenenergies, eigenstates
    """
    energy, psi = np.linalg.eigh(
        np.diag(
            np.array(
                [
                    (k + 2.0 * n) ** 2 + (4.0 * v_short + v_long) / 2.0
                    for n in range(-lattice_dim, lattice_dim + 1)
                ]
            )
        )
        - v_long / 4.0 * np.exp(+1j * phi) * np.eye(2 * lattice_dim + 1, k=-1)
        + v_short * np.eye(2 * lattice_dim + 1, k=-2)
    )
    return energy, psi


@jit(
    numba.types.Tuple((float64[:], complex128[:, :]))(
        float64, float64, float64, int_, int_, int_
    )
)
def bloch_vec(v_long, v_short, phi, lattice_dim, k_dim, bands):
    """
    Calculates band structure and corresponding eigenstates.
    :param v_long: lattice depth of the long period lattice in Er_long
    :param v_short: lattice depth of the short period lattice in Er_short
    :param phi: relative superlattice phase (0: symmetric, pi/2: staggered)
    :param lattice_dim: 2 * lattice_dim + 1 is the system size
    :param k_dim: number of k states
    :param bands: number of bands
    :return: linear array containing sequence of energies and eigenstates
     for with b * k_dim + k for b in bands for k in k_states
    """
    psi = np.empty((bands * k_dim, 2 * lattice_dim + 1), dtype=complex128)
    energy = np.empty((bands * k_dim), dtype=float64)
    for ki, k in enumerate(np.linspace(-1 + 2 / k_dim, 1, k_dim)):
        e_k, psi_k = bloch_states(k, v_long, v_short, phi, lattice_dim)
        for b in range(bands):
            psi[b * k_dim + ki, :] = psi_k[:, b]
            energy[b * k_dim + ki] = e_k[b]
    return energy, psi


@njit(
    numba.types.Tuple((float64[:], complex128[:, :]))(
        float64, float64, float64, float64, float64, int_, int_
    )
)
def bloch_states_tilt(k, v_long, v_short, phi, v_tilt, tilt_period, lattice_dim):
    """
    Calculates spectrum and eigenstates of a period two superlattice.
    :param k: quasi momentum (-1, 1]
    :param v_long: lattice depth of the long period lattice in Er_long
    :param v_short: lattice depth of the short period lattice in Er_short
    :param phi: relative superlattice phase (0: symmetric, pi/2: staggered)
    :param v_tilt: tilt lattice depth
    :param tilt_period: half periode of the tilt lattice
    :param lattice_dim: 2 * lattice_dim + 1 is the system size
    :return: eigenenergies, eigenstates
    """
    energy, psi = np.linalg.eigh(
        np.diag(
            np.array(
                [
                    (k + 2.0 * n) ** 2 / (tilt_period) ** 2
                    + (4.0 * v_short + v_long) / 2.0
                    for n in range(-lattice_dim, lattice_dim + 1)
                ]
            )
        )
        - v_tilt
        * tilt_period
        / np.pi
        / 2
        * np.exp(+1j * np.pi / 2)
        * np.eye(2 * lattice_dim + 1, k=-1)
        + v_long
        / 4.0
        * np.exp(+1j * phi)
        * np.eye(2 * lattice_dim + 1, k=-1 * tilt_period)
        + v_short * np.eye(2 * lattice_dim + 1, k=-2 * tilt_period)
    )
    return energy, psi


@jit(
    numba.types.Tuple((float64[:], complex128[:, :]))(
        float64, float64, float64, float64, int_, int_, int_
    )
)
def bloch_vec_tilt(v_long, v_short, phi, v_tilt, lattice_dim, k_dim, bands):
    """
    Calculates band structure and corresponding eigenstates.
    :param v_long: lattice depth of the long period lattice in Er_long
    :param v_short: lattice depth of the short period lattice in Er_short
    :param phi: relative superlattice phase (0: symmetric, pi/2: staggered)
    :param v_tilt: tilt lattice depth
    :param lattice_dim: 2 * lattice_dim + 1 is the system size
    :param k_dim: number of k states
    :param bands: number of bands (even) is 2 * tilt_period
    :return: linear array containing sequence of energies and eigenstates
     for with b * k_dim + k for b in bands for k in k_states
    """
    tilt_period = bands // 2
    psi = np.empty((bands * k_dim, 2 * lattice_dim + 1), dtype=complex128)
    energy = np.empty((bands * k_dim), dtype=float64)
    for ki, k in enumerate(np.linspace(-1 + 2 / k_dim, 1, k_dim)):
        e_k, psi_k = bloch_states_tilt(
            k, v_long, v_short, phi, v_tilt, tilt_period, lattice_dim
        )
        for b in range(bands):
            psi[b * k_dim + ki, :] = psi_k[:, b]
            energy[b * k_dim + ki] = e_k[b]
    return energy, psi


@njit(complex128(int_, int_, int_, int_, int_))
def _helper_xmat(n: int, m: int, nn: int, mm: int, k_dim: int):
    """
    Helper function to generate xmat
    :param n: site index of first state
    :param m: k-state index of first state
    :param nn: site index of second state
    :param mm: k-state index of second state
    :param k_dim: number of k states
    :return: complex number
    """
    m = m % k_dim
    mm = mm % k_dim
    c = (
        0.5 * (k_dim - 1)
        if (n == nn and m == mm)
        else (1j / (2 * np.pi * (n - nn + (m - mm) / k_dim)))
    )
    return np.exp(1j * np.pi / k_dim * (m - mm)) * (-1) ** (n - nn) * c


@njit(complex128[:, :](complex128[:, :], int_, int_, int_))
def xmat(bv, lattice_dim, k_dim, bands):
    """
    Transformation matrix between Bloch and position space
    (PhD thesis Ulf Bissbort, 2012, Frankfurt)
    :param bv: Bloch vector (eigenstates)
    :param lattice_dim: 2 * lattice_dim + 1 is the system size
    :param k_dim: number of k states
    :param bands: number of bands
    :return: x-matrix
    """
    x = np.zeros((bands * k_dim, bands * k_dim), dtype=complex128)
    for m in range(bands * k_dim):
        for mm in range(bands * k_dim):
            for n in range(2 * lattice_dim + 1):
                for nn in range(2 * lattice_dim + 1):
                    x[m, mm] += (
                        np.conj(bv[m, n])
                        * bv[mm, nn]
                        * _helper_xmat(n, m, nn, mm, k_dim)
                    )
    return x


# No jit numpy.einsum() not supported
def overlap(
    x,
    be,
    bv,
    lattice_dim,
    k_dim,
    bands,
    int_step,
    int_range,
    wavelength_nm,
    tunneling_only=False,
):
    """
    Calculates different wave function overlap between Wannier functions
    :param x: x matrix from xmat(...)
    :param be: Bloch energy from bloch_vec(...)
    :param bv: Bloch vector from bloch_vec(...)
    :param lattice_dim: 2 * lattice_dim + 1 is the system size
    :param k_dim: number of k states
    :param bands: number of bands
    :param int_step: integration step size
    :param int_range: size of integration range
    :param wavelength_nm: wavelength of the long-period retro-reflected lattice
    :return: overlap (ovp_j, ovp_d, ovp_u, ovp_ulr, ovp_jdd0, ovp_jdd1)
    """
    # diagonalize x matrix for position basis
    pos, pos_vec = np.linalg.eigh(x)
    # region and position where the tunneling, tilt and interaction is evaluated
    eval_idx = bands * k_dim // 2
    rr = range(bands * k_dim // 2, bands * k_dim // 2 + 2)  # return range
    # calculate tilt [4Er]
    ovp_d = (
        np.abs(np.roll(pos_vec, 1, axis=1) ** 2).T.dot(be)
        - np.abs(pos_vec ** 2).T.dot(be)
    )[eval_idx] / (wavelength_nm * 10 ** -9) ** 2
    # calculate tunneling rates [4Er * lambda^2 = 2*h^2/m]
    ovp_j = (
        np.abs(
            np.einsum("ip,i,ip->p", np.conj(pos_vec), be, np.roll(pos_vec, +1, axis=1))
        )[rr]
        / (wavelength_nm * 10 ** -9) ** 2
    )
    # interrupt when only tunneling ifo is required
    if tunneling_only:
        return np.hstack((ovp_j, ovp_d))
    #
    # evaluation range for integration
    eval_range = (
        np.maximum(pos[eval_idx] - 0.5 * int_range, np.amin(pos)),
        np.minimum(pos[eval_idx] + 0.5 * int_range, np.amax(pos)),
    )
    # generate Bloch waves for q and n at real space positions in the integration range
    ks = np.tile(np.linspace(-1 + 2 / k_dim, 1, k_dim), bands)
    bloch_waves = np.array(
        [
            [
                [
                    np.exp(1j * (q + 2 * n) * np.pi * z)
                    for n in range(-lattice_dim, lattice_dim + 1)
                ]
                for q in ks
            ]
            for z in np.arange(*eval_range, int_step)
        ],
        dtype="complex128",
    )
    # calculate tilt [4Er]
    ovp_d = (
        np.abs(np.roll(pos_vec, 1, axis=1) ** 2).T.dot(be)
        - np.abs(pos_vec ** 2).T.dot(be)
    )[eval_idx] / (wavelength_nm * 10 ** -9) ** 2
    # calculate tunneling rates [4Er * lambda^2 = 2*h^2/m]
    ovp_j = (
        np.abs(
            np.einsum("ip,i,ip->p", np.conj(pos_vec), be, np.roll(pos_vec, +1, axis=1))
        )[rr]
        / (wavelength_nm * 10 ** -9) ** 2
    )
    # calculate interaction u, next neighbour interaction ulr,
    # and density dependent tunneling rates jdd*
    # product of three in units of [4pi*h_bar^2*as/m*2^3*ds^3 / (2/lambda)^3 = h*as/m/pi]
    ovp_dens_0 = np.einsum("ip,ij,xij->xp", pos_vec, bv, bloch_waves)
    ovp_dens_1 = np.einsum(
        "ip,ij,xij->xp", np.roll(pos_vec, +1, axis=1), bv, bloch_waves
    )
    ovp_u = (
        np.abs(np.sum(abs(ovp_dens_0) ** 4, axis=0) * int_step / k_dim ** 2)[rr]
        * 2
        / (wavelength_nm * 10 ** -9)
    )
    ovp_ulr = (
        np.abs(
            np.sum(np.conj(ovp_dens_0) ** 2 * ovp_dens_1 ** 2, axis=0)
            * int_step
            / k_dim ** 2
        )[rr]
        * 2
        / (wavelength_nm * 10 ** -9)
    )
    ovp_jdd0 = (
        np.abs(
            np.sum(np.conj(ovp_dens_0) ** 2 * ovp_dens_0 * ovp_dens_1, axis=0)
            * int_step
            / k_dim ** 2
        )[rr]
        * 2
        / (wavelength_nm * 10 ** -9)
    )
    ovp_jdd1 = (
        np.abs(
            np.sum(np.conj(ovp_dens_1) ** 2 * ovp_dens_0 * ovp_dens_1, axis=0)
            * int_step
            / k_dim ** 2
        )[rr]
        * 2
        / (wavelength_nm * 10 ** -9)
    )
    # NOTE: Returning the absolute value is a bit dangerou2s in a sense that it hides
    # possibly large imaginary contributions. These contributions are typically reduced
    # for larger k_dim. However, the calculations are more computationally intense.
    # Furthermore, the absolute value in mostly insensitive.
    return np.hstack((ovp_j, ovp_d, ovp_u, ovp_ulr, ovp_jdd0, ovp_jdd1))


def ho_overlap(omega, wavelength, m):
    ovp_j = (
        (m * omega)
        / (16 * pconst.h ** 2 * np.pi)
        * np.exp(-1 * (np.pi * m * wavelength ** 2 * omega) / (8 * pconst.h))
        * (
            m
            * wavelength ** 2
            * omega
            * (
                1
                + np.exp(-2 * np.pi * pconst.h / (m * wavelength ** 2 * omega))
                - np.pi
            )
            + 4 * pconst.h
        )
    )
    ovp_j = (
        (m * omega)
        / (16 * pconst.h ** 2 * np.pi)
        * np.exp(
            -2 * pconst.h * np.pi / (m * wavelength ** 2 * omega)
            - m * np.pi * wavelength ** 2 * omega / (8 * pconst.h)
        )
        * (
            2 * m * wavelength ** 2 * omega
            + (4 * pconst.h + m * wavelength ** 2 * omega * (np.pi - 2))
            * np.exp(2 * pconst.h * np.pi / (m * wavelength ** 2 * omega))
        )
    )
    ovp_u = np.sqrt(m * omega / pconst.h)
    ovp_ulr = np.sqrt(m * omega / pconst.h) * np.exp(
        -np.pi * m * omega * wavelength ** 2 / (4 * pconst.h)
    )
    ovp_jdd = np.sqrt(m * omega / pconst.h) * np.exp(
        -3 * np.pi * m * omega * wavelength ** 2 / (16 * pconst.h)
    )
    return np.array(
        [
            ovp_j,
            ovp_j,
            0,
            ovp_u,
            ovp_u,
            ovp_ulr,
            ovp_ulr,
            ovp_jdd,
            ovp_jdd,
            ovp_jdd,
            ovp_jdd,
        ]
    )


@attr.s
class Wannier:
    lattice_dim = attr.ib(default=10)
    k_dim = attr.ib(default=6)  # needs to be even!
    bands = attr.ib(default=2)
    integration_steps = attr.ib(default=0.03)
    integration_range = attr.ib(default=3.0)
    m = attr.ib(default=cs.m)
    a_s = unit_value(unit=u.m, default=5.6092e-9 * u.m)  # default for Rb87

    @property
    def m_in_u(self):
        return (self.m / u.u).to(1).value

    def tunneling(self, v_long, v_short, phi, wavelength: u.nm):
        """
        Calculates the tunneling rates and tilt in a superlattice (J0, J1, Delta)
        :param v_long: lattice depth of the long period lattice in Er_long
        :param v_short: lattice depth of the short period lattice in Er_short
        :param phi: relative superlattice phase (0: symmetric, pi/2: staggered)
        :param wavelength: wavelength of the long-period retro-reflected lattice in nm
        :return: (J0, J1, Delta)/h in Hz
        """
        wavelength = wavelength.to(u.nm).value
        be, bv = bloch_vec(
            v_long, v_short, phi, self.lattice_dim, self.k_dim, self.bands
        )
        x = xmat(bv, self.lattice_dim, self.k_dim, self.bands)
        result = overlap(
            x,
            be,
            bv,
            self.lattice_dim,
            self.k_dim,
            self.bands,
            self.integration_steps,
            self.integration_range,
            wavelength,
            tunneling_only=True,
        )[:3]
        return result * pconst.h / (2 * self.m_in_u * pconst.u)

    def gradient_tunneling(
        self, v_long, v_short, phi, v_tilt, wavelength: u.nm, period=8
    ):
        bands = 2 * period
        wavelength = wavelength.to(nm).value
        be, bv = bloch_vec_tilt(
            v_long, v_short, phi, v_tilt, self.lattice_dim, self.k_dim, bands
        )
        x = xmat(bv, self.lattice_dim, self.k_dim, bands)
        result = overlap(
            x,
            be,
            bv,
            self.lattice_dim,
            self.k_dim,
            bands,
            self.integration_steps,
            self.integration_range,
            wavelength,
            tunneling_only=True,
        )[:3]
        return result * pconst.h / (2 * self.m_in_u * pconst.u)

    def hubbard(
        self,
        vx_long=None,
        vy_long=None,
        vz_long=None,
        wavelength: u.nm = None,
        wavelength_x: u.nm = None,
        wavelength_y: u.nm = None,
        wavelength_z: u.nm = None,
        vx_short=0.0,
        vy_short=0.0,
        vz_short=0.0,
        phi_x=0.0,
        phi_y=0.0,
        phi_z=0.0,
        bands_x=None,
        bands_y=None,
        bands_z=None,
        omega_x=None,
        omega_y=None,
        omega_z=None,
    ):
        """
        :param vx_long: lattice depth of the long period lattice in Er_long along x
        :param vy_long: ... along y
        :param vz_long: ... along z
        :param wavelength: global wavelength of the long-period retro-reflected lattice in nm
        :param wavelength_x: overrides wavelength of the long-period retro-reflected lattice in nm along x
        :param wavelength_y: overrides along y
        :param wavelength_z: overrides along z
        :param vx_short: lattice depth of the short period lattice in Er_short along x
        :param vy_short: ... along y
        :param vz_short: ... along z
        :param phi_x: relative superlattice phase (0: symmetric, pi/2: staggered) along x
        :param phi_y: ... along y
        :param phi_z: ... along z
        :param bands_x: number of bands used for the calculation {1, 2} along x
        :param bands_y: ... along y
        :param bands_z: ... along z
        :param bands_x: use harmonic oscillator frequency along x
        :param bands_y: ... along y
        :param bands_z: ... along z
        :return: a dict with Hubbard parameters per axis (x, y, z)
        with keys ('j0', 'j1', 'delta', 'u0', 'u1', 'ulr0', 'ulr1', 'jdd00', 'jdd01', 'jdd10', 'jdd11')
        j0, j1: tunneling rates
        delta: tilt between neighbouring sites E1-E0
        u0, u1: on-site interaction energy between two particles on site 0 and 1
        ulr1, ulr2: next-neighbour interaction
        jdd**: density-dependent hopping first index where the particles are,
        second index over which barrier it is hopping equivalent to tunneling rates
        """
        wavelength_x = (
            wavelength_x.to(u.nm).value
            if wavelength_x is not None
            else wavelength.to(u.nm).value
        )
        wavelength_y = (
            wavelength_y.to(u.nm).value
            if wavelength_y is not None
            else wavelength.to(u.nm).value
        )
        wavelength_z = (
            wavelength_z.to(u.nm).value
            if wavelength_z is not None
            else wavelength.to(u.nm).value
        )

        bands_x = bands_x if bands_x is not None else self.bands
        bands_y = bands_y if bands_y is not None else self.bands
        bands_z = bands_z if bands_z is not None else self.bands

        if omega_x is None:
            be, bv = bloch_vec(
                vx_long, vx_short, phi_x, self.lattice_dim, self.k_dim, bands_x
            )
            x = xmat(bv, self.lattice_dim, self.k_dim, bands_x)
            ovp_x = overlap(
                x,
                be,
                bv,
                self.lattice_dim,
                self.k_dim,
                bands_x,
                self.integration_steps,
                self.integration_range,
                wavelength_x,
            )
        else:
            ovp_x = ho_overlap(
                omega=omega_x, wavelength=wavelength_x * 1e-9, m=self.m_in_u * pconst.u
            )

        if omega_y is None:
            be, bv = bloch_vec(
                vy_long, vy_short, phi_y, self.lattice_dim, self.k_dim, bands_y
            )
            x = xmat(bv, self.lattice_dim, self.k_dim, bands_y)
            ovp_y = overlap(
                x,
                be,
                bv,
                self.lattice_dim,
                self.k_dim,
                bands_y,
                self.integration_steps,
                self.integration_range,
                wavelength_y,
            )
        else:
            ovp_y = ho_overlap(
                omega=omega_y, wavelength=wavelength_y * 1e-9, m=self.m_in_u * pconst.u
            )

        if omega_z is None:
            be, bv = bloch_vec(
                vz_long, vz_short, phi_z, self.lattice_dim, self.k_dim, bands_z
            )
            x = xmat(bv, self.lattice_dim, self.k_dim, bands_z)
            ovp_z = overlap(
                x,
                be,
                bv,
                self.lattice_dim,
                self.k_dim,
                bands_z,
                self.integration_steps,
                self.integration_range,
                wavelength_z,
            )
        else:
            ovp_z = ho_overlap(
                omega=omega_z, wavelength=wavelength_z * 1e-9, m=self.m_in_u * pconst.u
            )

        keys = (
            "j0",
            "j1",
            "delta",
            "u0",
            "u1",
            "ulr0",
            "ulr1",
            "jdd00",
            "jdd01",
            "jdd10",
            "jdd11",
        )

        res_x = np.hstack(
            (
                ovp_x[:3] * pconst.h / (2 * self.m_in_u * pconst.u),
                pconst.h
                * self.a_s
                / (self.m_in_u * pconst.u)
                / np.pi
                * ovp_x[3:]
                * ovp_y[3]
                * ovp_z[3],
            )
        )
        res_y = np.hstack(
            (
                ovp_y[:3] * pconst.h / (2 * self.m_in_u * pconst.u),
                pconst.h
                * self.a_s
                / (self.m_in_u * pconst.u)
                / np.pi
                * ovp_x[3]
                * ovp_y[3:]
                * ovp_z[3],
            )
        )
        res_z = np.hstack(
            (
                ovp_z[:3] * pconst.h / (2 * self.m_in_u * pconst.u),
                pconst.h
                * self.a_s
                / (self.m_in_u * pconst.u)
                / np.pi
                * ovp_x[3]
                * ovp_y[3]
                * ovp_z[3:],
            )
        )
        return dict(zip(keys, res_x)), dict(zip(keys, res_y)), dict(zip(keys, res_z))


if __name__ == "__main__":
    from cs.units import um

    w = Wannier()

    u1 = w.hubbard(
        vx_long=20.0,
        vx_short=10.0,
        phi_x=0.1,
        wavelength_x=1534,
        vy_long=0.0,
        vy_short=80.0,
        phi_y=0.0,
        wavelength_y=1534,
        vz_long=0.0,
        vz_short=80,
        phi_z=0.0,
        wavelength_z=1534,
    )
    print(u1)
    u2 = w.hubbard(
        vx_long=20.0,
        vx_short=10.0,
        phi_x=0.1,
        wavelength_x=1534,
        vy_long=80.0,
        vy_short=0.0,
        phi_y=0.0,
        wavelength_y=767,
        bands_y=1,
        vz_long=80.0,
        vz_short=0,
        phi_z=0.0,
        wavelength_z=767,
        bands_z=1,
    )
    print(u2)
    print(u1[0]["u0"] / u2[0]["u0"])

    print(w.tunneling(20, 10, 0, 1.534 * um))
    print(w.tunneling(20, 10, 0, 1534 * nm))
    print(w.tunneling(20, 10, np.pi / 2, 1534 * nm))

    w.k_dim = 8
    w.lattice_dim = 64
    print(w.gradient_tunneling(20, 10, 0, 2, 1534 * nm, 8))
