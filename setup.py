#!/usr/bin/env python3

from setuptools import find_namespace_packages, setup

with open("requirements.txt", "r") as f:
    requirements = f.read().splitlines()

setup(
    name="cs-lib",
    version="1.1.0",
    packages=find_namespace_packages(include=["cs.*"]),
    install_requires=requirements,
    license="GPL-3",
    description="Support code developed at the Caesium lab",
)
