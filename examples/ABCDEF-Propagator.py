#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

from cs.optics.ABCDEF import Beam, freeSpace, propagation, thinLens
from cs.units import deg, mm, nm

path = (
    freeSpace(100 * mm),
    thinLens(200 * mm, x=-0.1 * mm, theta=5 * deg),
    freeSpace(400 * mm),
    thinLens(200 * mm),
    freeSpace(300 * mm),
)
beam = Beam(waist=1 * mm, x=0.2 * mm, wavelength=1064 * nm)

p = propagation(beam, *path)
zs = np.linspace(0, 700, 1000) * mm
p(zs).plot()
plt.show()
