from itertools import product

import numpy as np
import pytest

from cs.constants import mu0
from cs.magnetic.fields import *
from cs.units import A, G, Quantity, cm, km, m, mm, um


def eval_single_loop(radius, distance):
    coil = Coil((0, 0, 0) * m, (0, 0, 1), radius=radius)
    d = distance.to_value(m)
    position = np.mgrid[0:0:1j, 0:0:1j, d:d:1j] * m
    return np.linalg.norm(coil.evaluate(position).squeeze().to_value(G / A)) * G / A


def eval_coil_polar(radius, distance):
    return coil_polar(0 * m, distance, radius)[1]


def eval_coil_cartesian(radius, distance):
    d = distance.to_value(m)
    position = np.mgrid[0:0:1j, 0:0:1j, d:d:1j] * m
    return coil_cartesian(position, radius)[2]


def eval_coil_generic(radius, distance):
    d = distance.to_value(m)
    position = np.mgrid[0:0:1j, 0:0:1j, d:d:1j] * m
    return coil(position, (0, 0, 0) * m, (0, 0, 1), radius)[2]


@pytest.mark.parametrize(
    "fun", [eval_coil_polar, eval_coil_cartesian, eval_coil_generic, eval_single_loop]
)
@pytest.mark.parametrize("radius", (0.01 * km, 1 * m, 1000 * mm, 1 * um))
@pytest.mark.parametrize("distance", (1 * m, 0.1 * km, 0.1 * mm))
def test_on_axis_single_loop(fun, radius, distance):
    expected = mu0 * radius ** 2 / (2 * np.power(radius ** 2 + distance ** 2, 3 / 2))
    calculated = fun(radius, distance).squeeze()
    print(expected)
    print(calculated)
    assert np.isclose(expected.to_value(G / A), calculated.to_value(G / A))


@pytest.mark.parametrize("radius", (1 * m, 2 * mm, 0.1 * m, 1000 * mm))
def test_helmholtz(radius):
    coils = [
        Coil((0 * m, 0 * m, radius / 2), (0, 0, 1), radius),
        Coil((0 * m, 0 * m, -radius / 2), (0, 0, 1), radius),
    ]
    expected = np.power(4 / 5, 3 / 2) * mu0 / radius
    pos = np.mgrid[0:0:1j, 0:0:1j, 0:0:1j] * m
    calculated = evaluate(coils, pos).squeeze()
    assert calculated[0].to_value(T / A) == 0
    assert calculated[1].to_value(T / A) == 0
    assert np.isclose(calculated[2].to_value(T / A), expected.to_value(T / A))


@pytest.mark.parametrize("radius", (1 * m, 2 * mm, 0.1 * m, 1000 * mm))
def test_xaxis_rotated_helmholtz(radius):
    coils = [
        Coil((radius / 2, 0 * m, 0 * m), (1, 0, 0), radius),
        Coil((-radius / 2, 0 * m, 0 * m), (1, 0, 0), radius),
    ]
    expected = np.power(4 / 5, 3 / 2) * mu0 / radius
    pos = np.mgrid[0:0:1j, 0:0:1j, 0:0:1j] * m
    calculated = evaluate(coils, pos).squeeze()
    print(expected.to(G / A))
    print(calculated[0].to(G / A))
    assert calculated[1].to_value(T / A) == 0
    assert calculated[2].to_value(T / A) == 0
    assert np.isclose(calculated[0].to_value(T / A), expected.to_value(T / A))


@pytest.mark.parametrize("radius", (1 * m, 2 * mm, 0.1 * m, 1000 * mm))
def test_yaxis_rotated_helmholtz(radius):
    coils = [
        Coil((0 * m, radius / 2, 0 * m), (0, 1, 0), radius),
        Coil((0 * m, -radius / 2, 0 * m), (0, 1, 0), radius),
    ]
    expected = np.power(4 / 5, 3 / 2) * mu0 / radius
    pos = np.mgrid[0:0:1j, 0:0:1j, 0:0:1j] * m
    calculated = evaluate(coils, pos).squeeze()
    print(expected.to(G / A))
    print(calculated[1].to(G / A))
    assert calculated[0].to_value(T / A) == 0
    assert calculated[2].to_value(T / A) == 0
    assert np.isclose(calculated[1].to_value(T / A), expected.to_value(T / A))


@pytest.mark.parametrize("radius", (1 * m, 2 * mm, 0.1 * m, 1000 * mm))
@pytest.mark.parametrize("z", np.random.rand(10))
def test_helmholtz_axis(radius, z):
    z = (z - 1 / 2) * radius * 4
    coils = [
        Coil((0 * m, 0 * m, radius / 2), (0, 0, 1), radius),
        Coil((0 * m, 0 * m, -radius / 2), (0, 0, 1), radius),
    ]

    def expected(x):
        R = radius
        coil1 = 1 / np.power(x ** 2 - R * x + 5 / 4 * R ** 2, 3 / 2)
        coil2 = 1 / np.power(x ** 2 + R * x + 5 / 4 * R ** 2, 3 / 2)
        return mu0 / 2 * radius ** 2 * (coil1 + coil2)

    pos = np.mgrid[0:0:1j, 0:0:1j, z.to_value(m) : z.to_value(m) : 1j] * m
    calculated = evaluate(coils, pos).squeeze()
    expd = expected(z)
    print(z)
    print(expd.to(G / A))
    print(calculated[2].to(G / A))
    assert calculated[0].to_value(T / A) == 0
    assert calculated[1].to_value(T / A) == 0
    assert np.isclose(calculated[2].to_value(T / A), expd.to_value(T / A))


@pytest.mark.parametrize("radius", (1 * m, 2 * mm, 0.1 * m, 1000 * mm))
@pytest.mark.parametrize("z", np.random.rand(10))
def test_anti_helmholtz_axis(radius, z):
    z = (z - 1 / 2) * radius * 4
    distance = np.sqrt(3) * radius
    coils = [
        Coil((0 * m, 0 * m, distance / 2), (0, 0, -1), radius),
        Coil((0 * m, 0 * m, -distance / 2), (0, 0, 1), radius),
    ]

    def expected(x):
        R = radius
        coil1 = 1 / np.power(R ** 2 + (x + distance / 2) ** 2, 3 / 2)
        coil2 = 1 / np.power(R ** 2 + (x - distance / 2) ** 2, 3 / 2)
        return mu0 / 2 * radius ** 2 * (coil1 - coil2)

    pos = np.mgrid[0:0:1j, 0:0:1j, z.to_value(m) : z.to_value(m) : 1j] * m
    calculated = evaluate(coils, pos).squeeze()
    expd = expected(z)
    print(z)
    print(expd.to(G / A))
    print(calculated[2].to(G / A))
    assert calculated[0].to_value(T / A) == 0
    assert calculated[1].to_value(T / A) == 0
    assert np.isclose(calculated[2].to_value(T / A), expd.to_value(T / A))


@pytest.mark.parametrize("radius", (1 * m, 2 * mm, 0.1 * m, 1000 * mm))
def test_anti_helmholtz_gradient(radius, direction=(0, 0, 1)):
    direction = np.asanyarray(direction)
    distance = np.sqrt(3) * radius
    distance_v = direction * distance / 2
    coils = [Coil(distance_v, direction, radius), Coil(-distance_v, -direction, radius)]
    expected = np.sqrt(6912 / 16807) * mu0 / radius ** 2
    print(expected.to(G / cm / A))
    pos = np.mgrid[0:0:1j, 0:0:1j, -1:1:3j] * radius / 1000
    B = evaluate(coils, pos).squeeze()
    calculated = np.gradient(B[2], axis=-1) / (pos[2, 0, 0, 1] - pos[2, 0, 0, 0])
    print(calculated[1].to(G / cm / A))
    assert np.all(
        np.isclose(expected.to_value(T / m / A), calculated[1].to_value(T / m / A))
    )


@pytest.mark.parametrize("radius", (1 * m, 2 * mm, 0.1 * m, 1000 * mm))
def test_compare_round_and_roundrect(radius):
    roundrect = SquareCoil(
        (0 * m, 0 * m, 0 * m),
        (0, 0, 1),
        dx=2 * radius,
        dy=2 * radius,
        corner_radius=radius,
        wires_per_corner=8,
    )
    circ = Coil((0 * m, 0 * m, 0 * m), (0, 0, 1), radius=radius)

    pos = np.mgrid[-1:1:5j, -1:1:7j, -1:1:9j] * radius / 10

    B_r = evaluate((roundrect,), pos)
    B_c = evaluate((circ,), pos)

    assert not np.any(np.isnan(B_r))
    assert not np.any(np.isinf(B_r))

    difference = B_c / B_r - 1
    assert np.all(np.logical_or(difference < 1e-2, np.isnan(difference)))
