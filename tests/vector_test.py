import numpy as np
import pytest

from cs.units import deg, m
from cs.vector import in_translated_rotated_reference_frame, rotation_from_vectors


@pytest.mark.parametrize("v", np.random.rand(10, 3))
def test_rotation_identity(v):
    print(v)
    assert np.all(rotation_from_vectors(v, v) == np.eye(3))


@pytest.mark.parametrize("v1, v2", np.random.rand(20, 3).reshape(10, 2, 3))
def test_double_rotation(v1, v2):
    print(v1)
    print(v2)
    mat = rotation_from_vectors(v1, v2)
    anti_mat = rotation_from_vectors(v2, v1)
    assert np.all(np.isclose(np.linalg.inv(mat), anti_mat))


def test_translation_rotation_quantity_preservation():
    normal = (1, 1, 1)
    position = np.asarray((1, 2, 3)) * m
    coords = np.mgrid[-1:1:11j, -1:1:11j, -1:1:11j] * m
    res = in_translated_rotated_reference_frame(position, normal, coords, lambda x: x)
    assert res.unit == m
