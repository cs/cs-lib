def test_namespace():
    import cs.constants

    # when it is not a namespace it is a simple list
    assert type(cs.__path__) != list
