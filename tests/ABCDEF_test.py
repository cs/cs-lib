from itertools import product

import pytest

from cs.optics.ABCDEF import *
from cs.units import m, mm, nm


def test_zero_propagation():
    beam = Beam(waist=1 * mm, wavelength=1064 * nm)
    prop = propagation(beam, freeSpace(0 * mm))
    p0 = prop(0 * m)
    print(beam)
    print(p0.theta_x)
    assert beam.R == p0.R
    assert beam.theta_x == p0.theta_x
    assert beam.x == p0.x
    assert beam.waist == p0.waist


@pytest.mark.parametrize("waist", (0.01 * mm, 0.1 * mm, 1 * m, 10 * nm))
@pytest.mark.parametrize("wavelength", [850, 1064] * nm)
def test_ABCD_rayleigh_length(waist, wavelength):
    beam = Beam(waist=waist, wavelength=wavelength)
    rayleigh_length = pi * (waist) ** 2 / wavelength
    p = propagation(beam, freeSpace(rayleigh_length))
    print(rayleigh_length.to(mm))
    print(p(rayleigh_length).waist.to(mm))
    print(waist * np.sqrt(2))
    assert np.isclose(
        p(rayleigh_length).waist.to_value(m), waist.to_value(m) * np.sqrt(2)
    )
