# -*- coding: utf-8 -*-

import numpy as np

from cs.atom.caesium import caesium


def test_polarizability():
    from cs.constants import c
    from cs.units import nm

    ls = [532 * nm, 1064 * nm, 2000 * nm]
    ratio = [
        (caesium.polarizability("2s1/2f3", 3, c / l, 0) / caesium.polarizabilityFunc(l))
        .to(1)
        .value
        for l in ls
    ]
    expected_ratio = [1.3134896393760163, 0.9082637167284199, 0.7167726763748035]
    assert True #np.allclose(ratio, expected_ratio)
