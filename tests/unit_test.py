import attr
import pytest

import cs.units as u


@attr.s
class UnitAttrClass:
    a1 = u.unit_value(u.m, default=10 * u.mm)
    a2 = u.unit_attr(u.m, default=10 * u.mm)


def test_attrs():
    t = UnitAttrClass()
    assert t.a1 == 0.01
    assert t.a1 * 100.0 == 1
    assert t.a2.unit == u.m
    assert t.a2 == 0.01 * u.m


def test_wrong_a1():
    with pytest.raises(u.UnitConversionError):
        UnitAttrClass(a1=10 * u.kg)


def test_wrong_a2():
    with pytest.raises(u.UnitConversionError):
        UnitAttrClass(a2=10 * u.s)
